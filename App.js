import React, { Fragment } from 'react';
import {
  SafeAreaView,
  useColorScheme
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { ThemeProvider } from 'styled-components';
import NavBar from '@commons/navbar';
import { navigationRef } from '@commons/navbar/navigationRef';
import { lightTheme, darkTheme, mycolorSchema, mycolorSchemaDark } from '@utils/constants';
import { Provider } from '@context';
import { Provider as ReduxProvider } from 'react-redux'
import { ToastProvider } from '@etaui/toast/ToastProvider'
import { store } from './src/store'

const App: () => React$Node = () => {
  const colorSchema = useColorScheme();
  // console.disableYellowBox = true

  return (
    <Fragment>
      <SafeAreaView style={{ flex: 0, backgroundColor: colorSchema == 'dark' ? lightTheme.PRIMARY_COLOR : lightTheme.PRIMARY_COLOR }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: colorSchema == 'dark' ? lightTheme.PRIMARY_COLOR : lightTheme.PRIMARY_COLOR }}>
          <ThemeProvider theme={colorSchema == 'dark' ? lightTheme : lightTheme}>
            <Provider>
              <ReduxProvider store={store}>
                <ToastProvider>
                  <NavigationContainer ref={navigationRef} theme={colorSchema == 'dark' ? mycolorSchema : mycolorSchema }>
                    <NavBar />
                  </NavigationContainer>
                </ToastProvider>
              </ReduxProvider>
            </Provider>
          </ThemeProvider>
      </SafeAreaView>
    </Fragment>
  );
};

export default App;

