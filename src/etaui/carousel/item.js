import React from 'react';
import {Dimensions} from 'react-native';
import styled from 'styled-components';
import {ETASimpleText} from '@etaui';

const {width, height} = Dimensions.get('window');

const Root = styled.View`
  width: ${width}px;
  height: 170px;
  backgroundColor: ${(props) => props.theme.PRIMARY_TEXT_BACKGROUND_COLOR};
  shadowColor: #000;
  shadowOpacity: 0;
  shadowRadius: 3px;
  elevation: 0;
`;
// shadowOffset: ${{width: 0.5, height: 0.5}};
const ItemImage = styled.Image`
  width: ${width}px;
  height: 170px;
`;
const ContentWrapper = styled.View`
  position: absolute;
  bottom: 10px;
  margin: 7px;
  left: 5px;
`;

const ETACarouselItem = ({item}) => {
  return (
    <Root
      style={{
        shadowOffset: {width: 0.5, height: 0.5},
      }}>
      <ItemImage source={{uri: item.image}} />
    </Root>
  );
};

export default ETACarouselItem;
