import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { View, TouchableWithoutFeedback, Text, Platform } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

const DateContainer = styled.View`
`;

const ETADatepicker = ({ today, placeholder, onChange, value }) => {
  const [ date, setDate ] = useState(new Date(1598051730000));
  const [ mode, setMode ] = useState('date');
  const [ show, setShow ] = useState(false);
  const [ birthdateText, setbirthdateText ] = useState(placeholder)

  useEffect(() => {
    let isUnmount = false
    const today = new Date();
    let yearconvertedewe = today.getFullYear();
    let monthconvertedewe = ('0' + (today.getMonth() + 1)).slice(-2)
    let dayconvertedewe = today.getDate()
    const eighteenyearsago = toTimestamp(yearconvertedewe - 18, monthconvertedewe, Platform.OS === 'ios' ? dayconvertedewe : dayconvertedewe + 1, 0,0,0)
    // console.log('datepicker 18+', new Date(eighteenyearsago));
    let dateplus18 = new Date(eighteenyearsago) 
    console.log('dateplus18', dateplus18);
  
    // return dateplus18
    setDate(dateplus18)
    return () => {
      isUnmount = true
    }
  }, [])

  const toTimestamp = ( year, month, day, hour, minute, second ) => {
      var datum = new Date(Date.UTC( year, month-1, day, hour, minute, second ));
      
      return datum.getTime();
  }

  ///////////////////////////////////////////////////////

  const _onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    let monthconverted = ('0' + (currentDate.getMonth() + 1)).slice(-2)
    let selectedDateFormated = `${currentDate.getDate()}/${monthconverted}/${currentDate.getFullYear()}` 
    console.log('date we: ', selectedDateFormated);
    
    setbirthdateText(selectedDateFormated)
    
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  return (
    <DateContainer>
        <View style={{
            backgroundColor: '#FFFFFF',
            borderRadius: 30,
            width: 300,
            height: 45,
            margin: 5,
            flexDirection: 'row',
            alignItems: 'center',
        }}>
            <TouchableWithoutFeedback onPress={showDatepicker}>
                <Text style={{ color: '#777', fontSize: 15, marginLeft: 15 }}>{birthdateText.toString()}</Text>
            </TouchableWithoutFeedback>
        </View>
        {show && (
          <DateTimePicker
            testID='dateTimePicker'
            timeZoneOffsetInMinutes={0}
            value={date}
            mode={mode}
            is24Hour={true}
            display='spinner'
            onChange={_onChange}
          />
        )}
        </DateContainer>
  );
};

export default ETADatepicker;