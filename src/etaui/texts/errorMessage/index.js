import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/native';
import { ETASimpleText } from '@etaui';

const Root = styled.View`
    flex: 1;
    marginVertical: 5px;
    paddingHorizontal: 10px;
`;

const ETAErrorMessage = ({ children, size }) => {
    const themeContext = useContext(ThemeContext);

    return (
        <>
            <Root>
                <ETASimpleText size={size} weight='700' color={themeContext.FAIL_COLOR} align={'left'}>
                    {children}
                </ETASimpleText>
            </Root>
        </>
    );
}

export default ETAErrorMessage;