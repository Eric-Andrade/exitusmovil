import React, { useContext } from 'react';
import { View } from 'react-native';
import styled, { ThemeContext } from 'styled-components/native';
import { Ionicons } from '@icons';
import RNPickerSelect, { defaultStyles } from 'react-native-picker-select';

const Root = styled.View`
    flex: 1;
    flexDirection: row;
    justifyContent: center;
    alignItems: center;
    alignSelf: center;
    alignContent: center;
    marginVertical: 3px;
    marginBottom: 4px;
    borderRadius: 30px;
`;
const PickerContainer = styled.View`
    backgroundColor: ${props => props.theme.THIRD_BACKGROUND_COLOR_LIGHT};
    borderRadius: 30px;
    marginHorizontal: 5px;
    paddingHorizontal: 5px;
    display: flex;
    justifyContent: center;
    alignItems: center;
    alignSelf: center;
    alignContent: center;
    height: 45px;
    width: 275px;
    borderWidth: 0;
    borderTopColor: transparent;
    borderTopWidth: 0
`;

const ETAPicker = ({ items, placeholder, onChange, selected }) => {
    const themeContext = useContext(ThemeContext);
 
    return (
        <Root>
            {
                items
                ?   <PickerContainer>
                        <View style={[defaultStyles.modalViewMiddle, { backgroundColor: 'transparent' }]}>
                            <RNPickerSelect
                                placeholder={{
                                    label: placeholder ? placeholder : selected,
                                    value: '',
                                    color: '#000',
                                }}
                                items={items}
                                value={selected}
                                onValueChange={onChange}
                                style={{                 
                                    placeholder: {
                                        fontSize: 15,
                                        flexDirection: 'column',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        color: '#777',
                                        backgroundColor: themeContext.THIRD_BACKGROUND_COLOR_LIGHT,
                                        borderRadius: 30,
                                        borderWidth: 0,
                                        borderTopColor: 'transparent',
                                        borderTopWidth: 0
                                    },
                                    inputIOS: {
                                        fontSize: 15,
                                        color: 'black',
                                        backgroundColor: themeContext.THIRD_BACKGROUND_COLOR_LIGHT,
                                        width: 245,
                                        height: 45,
                                        borderWidth: 0,
                                        borderColor: 'gray',
                                    },
                                    inputAndroid: {
                                        fontSize: 15,
                                        color: 'black',
                                        backgroundColor: themeContext.THIRD_BACKGROUND_COLOR_LIGHT,
                                        width: 230,
                                        height: 40,
                                        borderTopWidth: 0,
                                        borderColor: 'gray',
                                    },
                                    iconContainer: {
                                        top: 15,
                                        left: 220,
                                    },
                                }}
                                useNativeAndroidPickerStyle={false}
                                Icon={() => {
                                    return <Ionicons name='md-caret-down-sharp' size={15} color='#777' />
                                }}
                            />
                        </View>
                    </PickerContainer>
                :   null
            }
        </Root>
    );
}

export default ETAPicker;