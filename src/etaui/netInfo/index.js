import React, { useState, useEffect } from 'react';
import NetInfo from '@react-native-community/netinfo';
import styled from 'styled-components/native';
import { ETASimpleText } from '@etaui';

const Root = styled.SafeAreaView`
    backgroundColor: ${props => props.theme.SECONDARY_COLOR};
    paddingVertical: 5px;
`;

const ETANetInfo = () => {
    const [ isInternetReachable, setisInternetReachable ] = useState(false);

    useEffect(() => {
        let isUnmount = false
        const unsubscribe = NetInfo.addEventListener( state => {
            setisInternetReachable(state.isInternetReachable)
            console.log('isInternetReachable: ', state.isInternetReachable)
        });

        return () => {
            unsubscribe();
        }
        return () => {
          isUnmount = true
        }
    }, [])

    if (isInternetReachable) {
        return null
    }

    return (
        <Root>
            <ETASimpleText size={14} weight='400' color='white' align={'center'}>
                Sin internet. Reconectando...
            </ETASimpleText>
        </Root>
    );
}

export default ETANetInfo;