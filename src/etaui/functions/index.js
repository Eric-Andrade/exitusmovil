import truncateString from './truncateString'
import currencySeparator from './currencySeparator'

export { truncateString, currencySeparator }
