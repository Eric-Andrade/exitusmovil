import React from 'react';
import styled from 'styled-components/native';

const TextInput = styled.TextInput.attrs({
    // selectionColor: Platform.OS === 'ios' ? base.GRAYFBINPUTTEXT : base.GRAYFBINPUTTEXT,
    // autoCorrect: false,
})`
    width: ${props => props.width ? props.width : 200}px;
    height: ${props => props.height ? props.height : 40}px;
    alignSelf: center;
    borderBottomColor: gray;
    borderBottomWidth: 1px;
    fontSize: ${props => props.textsize ? props.textsize : 14}px;
    color: black;
    margin: 5px;
`;
    // backgroundColor: ${props => props.theme.GRAYLIGHT};

const ETATextInput = ({
    value,
    placeholder,
    placeholderTextColor,
    keyboardtype,
    autocapitalize,
    allowfontscaling,
    autocorrect,
    autofocus,
    bluronsubmit,
    carethidden,
    clearbuttonmode,
    contextmenuhidden,
    editable,
    enablesreturnkeyautomatically,
    underlinecolorandroid,
    keyboardappearance,
    maxlength,
    multiline,
    numberoflines,
    returnkeylabel,
    securetextentry,
    selectioncolor,
    spellcheck,
    textcontenttype,
    returnkeytype,
    textalign,
    textsize,
    height,
    width,
 }) => {
    return (
        <>
            <TextInput
                value={value}
                placeholder={placeholder}
                placeholderTextColor={placeholderTextColor ? placeholderTextColor : 'rgba(0, 255, 0, 1)'} //GRAYFBINPUTTEXT
                keyboardType={keyboardtype}
                autoCapitalize='none'
                allowFontScaling={true}
                autoCorrect={true}
                autoFocus={autofocus}
                blurOnSubmit={false}
                caretHidden={false}
                clearButtonMode={clearbuttonmode}
                contextMenuHidden={contextmenuhidden}
                editable={editable}
                enablesReturnKeyAutomatically={enablesreturnkeyautomatically}
                underlineColorAndroid='transparent'
                keyboardAppearance='dark'
                maxLength={maxlength}
                multiline={multiline}
                numberOfLines={numberoflines} //android
                returnKeyLabel={returnkeylabel} //android
                secureTextEntry={securetextentry} //password
                spellCheck={spellcheck}
                textContentType={textcontenttype}
                returnKeyType={returnkeytype}
                textsize={textsize}
                height={height}
                width={width}
                // selection='1, 4'//? no sé we xd
                // onBlur={text => this._onBlur(text)}
                // onChangeText={onchangetext}
                // onEndEditing={text => this._onEndEditing(text)}
                // onFocus={text => this._onFocus(text)}
                // ref={(input) => {this.emailInput = input }}
                // onKeyPress={}
                // onScroll={}
                />
        </>
    );
}

export default ETATextInput;
/*  Notes
autoCapitalize enum:('none', 'sentences', 'words', 'characters')

keyboardType enum:('default', 'email-address', 'numeric', 'phone-pad', 'ascii-capable', 'numbers-and-punctuation', 
                'url', 'number-pad', 'name-phone-pad', 'decimal-pad', 'twitter', 'web-search', 'visible-password')

textContentType enum:('none', 'URL', 'addressCity', 'addressCityAndState', 'addressState', 'countryName', 'creditCardNumber', 
'emailAddress', 'familyName', 'fullStreetAddress', 'givenName', 'jobTitle', 'location', 'middleName', 'name', 'namePrefix', 
'nameSuffix', 'nickname', 'organizationName', 'postalCode', 'streetAddressLine1', 'streetAddressLine2', 'sublocality', 
'telephoneNumber', 'username', 'password')

returnKeyType enum:('done', 'go', 'next', 'search', 'send', 'none', 'previous', 'default', 'emergency-call', 
                    'google', 'join', 'route', 'yahoo')
clearButtonMode enum:('never', 'while-editing', 'unless-editing', 'always')
secureTextEntry boolean: default false
*/