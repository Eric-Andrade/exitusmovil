import React, { useContext } from 'react';
import { Dimensions } from 'react-native';
import styled, { ThemeContext } from 'styled-components/native';
import CardHeader from './cardHeader';
import CardBody from './cardBody';
import CardBottom from './cardBottom';

const {width} = Dimensions.get('window');

const Root = styled.View`
    minHeight: 95px;
    width: ${width - 40}px;
    backgroundColor: ${props => props.theme.PRIMARY_TEXT_BACKGROUND_COLOR};
    marginVertical: 1px;
    borderRadius: 8px;
    paddingVertical: 2px;
    paddingHorizontal: 10px;
    borderLeftWidth: 2px;
    borderLeftColor: ${props => props.theme.FAIL_COLOR}
`;

const ETACard = ( props ) => {
    const themeContext = useContext(ThemeContext);
    
    return (
        <Root
            style={{
                borderLeftColor: props.nDeposit ?  themeContext.SUCCESS_COLOR : themeContext.REJECTED_COLOR,
            }}
            >
            <CardHeader creditid={props.iCreditId} status={props.nDeposit} nAmount={props.nAmount} />
            <CardBody {...props}/>
            {/* <CardBottom status={props.status} /> */}
        </Root>
    );
}

export default ETACard;