import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';
import { ETAButtonFilled, ETASimpleText } from '@etaui';
// import distanceInWordsToNow from 'date-fns/distance_in_words_to_now'
// import eoLocale from 'date-fns/locale/es';
import { base } from '@utils/constants';
import { currencySeparator } from '@functions'

const avatarSize = 35;
const avatarRadius = avatarSize / 2;

const Root = styled.View`
    flexDirection: row;
    alignItems: center;
    padding: 3px;
`;
const MetaContainer = styled.View`
    flex: 1;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: stretch;
    alignSelf: stretch;
    paddingTop: 6px;
    borderBottomWidth: 0px;
    borderColor: ${props => props.theme.PRIMARY_TEXT_COLOR_LIGHT};
`;
const MetaLeftContainer = styled.View`
    flex: 1;
    alignSelf: stretch;
    flexDirection: row;
    alignItems: center;
    justifyContent: flex-start;
`;
const MetaRightContainer = styled.View`
    flex: 1;
    alignSelf: center;
    flexDirection: row;
    alignItems: center;
    justifyContent: flex-end;
    marginVertical: 2px;
    marginHorizontal: 5px;
`;
const TagContainer = styled.View`
    flexDirection: row;
    borderRadius: 5px;
    borderWidth: 0.5px;
    paddingHorizontal: 3px;
    justifyContent: center;
    alignItems: center;
    marginHorizontal: 5px;
`;

const CardHeader = ({ creditid, nAmount, status }) => {
    const themeContext = useContext(ThemeContext);
    const navigation = useNavigation();

    const handleSubmit = () => {
        navigation.navigate('SuccessCreditScreen')
    }

    return(
        <Root>
            <MetaContainer>
                <MetaLeftContainer>
                    <ETASimpleText size={12} weight='700' color={themeContext.PRIMARY_TEXT_COLOR} align='left' >Crédito{' '}</ETASimpleText>
                    <ETASimpleText size={12} weight='500' color={themeContext.PRIMARY_TEXT_COLOR} align='left' >{(creditid)}</ETASimpleText>
                    <TagContainer style={{ borderColor: status ? '#61ca72' : '#ff6666' }}>
                        <ETASimpleText size={10} weight='100' color={ status ? '#61ca72' : '#ff6666' } align='center' >{ status ? 'renovar' : 'sin renovación'}</ETASimpleText>
                    </TagContainer>
                </MetaLeftContainer>
                <MetaRightContainer>
                    <ETASimpleText size={12} weight='700' color={themeContext.PRIMARY_TEXT_COLOR} align='left' >${currencySeparator(parseFloat(nAmount).toFixed(2))}</ETASimpleText>
                </MetaRightContainer>
            </MetaContainer>
        </Root>
    );
}

export default CardHeader;