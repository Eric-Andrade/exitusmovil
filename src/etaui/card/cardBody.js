import React, { useContext } from 'react';
import { Platform } from 'react-native';
import styled, { ThemeContext } from 'styled-components/native';
import { MaterialCommunityIcons, FontAwesome, Entypo } from '@icons';
import { ETASimpleText } from '@etaui';
import { currencySeparator, truncateString } from '@functions'

const Root = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;
const ContentContainer = styled.View`
    flex: 1;
    flexDirection: row;
    justifyContent: center;
    alignItems: center;
`;
const ContainerDataTop = styled.View`
    flex: 0.2;
    flexDirection: row;
    justifyContent: center;
    alignItems: center;
`;
const ContainerDataBottom = styled.View`
    flex: 0.2;
    flexDirection: row;
    justifyContent: center;
    alignItems: center;
`;

const CardBody = ({ vRejected, iPayments, nAmount, nPayment, nNetAmount, nDeposit, Frecuencia }) => {
    const themeContext = useContext(ThemeContext);
    console.log('CardBody: ', { vRejected, iPayments, nAmount, nPayment, nNetAmount, nDeposit, Frecuencia });
    return (
        <Root>     
            <ContainerDataTop>
                <ContentContainer>
                    <MaterialCommunityIcons name='calendar' color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} size={12} />
                    <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '800' : 'bold'} color={themeContext.PRIMARY_TEXT_COLOR} align='left' >{' '}Plazo</ETASimpleText>
                    <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '400' : '300'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' >{' '}{iPayments} meses</ETASimpleText>
                </ContentContainer>

                {/* <ContentContainer>
                    <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '800' : '600'} color={themeContext.PRIMARY_TEXT_COLOR} align='left' >{ status === 1 ? 'Monto a recibir: ' : 'Monto: ' }</ETASimpleText>
                    <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '400' : '300'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' >${nAmount}</ETASimpleText>
                </ContentContainer> */}

                {/* {
                status == 1 */}
                    <ContentContainer>
                        <FontAwesome name='money' color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} size={12} />
                        <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '800' : 'bold'} color={themeContext.PRIMARY_TEXT_COLOR} align='left' >{' '}Pago</ETASimpleText>
                        <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '400' : '400'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' >{' '}${currencySeparator(parseFloat(nPayment).toFixed(2))}</ETASimpleText>
                    </ContentContainer>
                    {/* : null
                } */}
            </ContainerDataTop>
            <ContainerDataBottom>
                {
                    !nDeposit
                    ?   <ContentContainer>
                            <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '800' : 'bold'} color={themeContext.PRIMARY_TEXT_COLOR} align='left' >{' '}Motivo</ETASimpleText>
                            <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '400' : '400'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' >{' '}{ truncateString(vRejected, 11) }</ETASimpleText>
                        </ContentContainer>
                    :   <ContentContainer>
                            <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '800' : 'bold'} color={themeContext.PRIMARY_TEXT_COLOR} align='left' >{' '}Monto</ETASimpleText>
                            <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '400' : '400'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' > ${currencySeparator(parseFloat(nNetAmount).toFixed(2))}</ETASimpleText>
                        </ContentContainer>
                }            
                {
                    !nDeposit
                    ?   <ContentContainer>
                            <Entypo name='circular-graph' color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} size={12} />
                            <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '800' : 'bold'} color={themeContext.PRIMARY_TEXT_COLOR} align='left' >{' '}Frecuencia</ETASimpleText>
                            <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '400' : '400'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' >{' '}{ Frecuencia }</ETASimpleText>
                        </ContentContainer>
                    :   <ContentContainer>
                            {/* <Entypo name='circular-graph' color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} size={12} /> */}
                            <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '800' : 'bold'} color={themeContext.PRIMARY_TEXT_COLOR} align='left' >{' '}Deposito</ETASimpleText>
                            <ETASimpleText size={11} weight={Platform.OS === 'ios' ? '400' : '400'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' > ${currencySeparator(parseFloat(nDeposit).toFixed(2))}</ETASimpleText>
                        </ContentContainer>
                }
            </ContainerDataBottom>
        </Root>
    );
}

export default CardBody;