import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/native';
import { ETASimpleText } from '@etaui';
import { FontAwesome, AntDesign, Entypo } from '@commons/Icons';
import { useNavigation } from '@react-navigation/native';

const Root = styled.View`
    flex: 1;
    height: 30px;
    width: 100%;
    flexDirection: column;
    justifyContent: flex-end;
    alignItems: flex-end;
    marginTop: 15px;
`;
const TagContainer = styled.TouchableOpacity`
    flexDirection: row;
    width: 85px;
    height: 26px;
    borderRadius: 4px;
    justifyContent: center;
    alignItems: center;
`;

const CardBottom = ({ status }) => {
    const themeContext = useContext(ThemeContext);
    const navigation = useNavigation();

    const handleSubmit = () => {
        navigation.navigate('SuccessCreditScreen')
    }

    return (
        <Root>
            {/* <ETAButtonFilled 
                title='RENOVAR MI CRÉDITO' 
                onPress={handleSubmit} 
                disabled={isSubmitting ? true : false} 
                colorButton={themeContext.SUCCESS_COLOR} 
                align={'center'}
                padding={30}
            /> */}
            <TagContainer 
                onPress={status === 1 ? handleSubmit : null}
                style={{ backgroundColor: status === 1 ? '#d5f3e7' : '#ffe6cc' }} >
                <ETASimpleText size={13} weight='700' color={ status === 1 ? '#61ca72' : '#ff6666' } align='center' >{ status === 1 ? 'renovar' : 'rechazado'}</ETASimpleText>
                {/* <Entypo name='chevron-small-right' size={20} color={ status === 1 ? '#40bf55' : '' }/> */}
            </TagContainer>
        </Root>
    );
}

export default CardBottom;