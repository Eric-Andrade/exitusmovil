// https://www.youtube.com/watch?v=K6X9Xcy6oio

import React, { memo } from 'react';
import styled from 'styled-components/native';
import { StyleSheet, Dimensions } from 'react-native';
import ETACircularProgressHalfCircle from './halfcircle';
import Animated, { multiply, interpolate, Extrapolate, lessThan } from 'react-native-reanimated';
import { transformOrigin } from 'react-native-redash';

const { width } = Dimensions.get('window')

const Root = styled.View`
`;
const CirclesContainer = styled.View`
`;
const ViewStroked = styled.View`
`;

const ETACircularProgress = memo(({ bg, fg, progress, size }) => {
    const radius = (width - 72);
    const PI = radius * Math.PI;
    // const theta = multiply(progress, 2 * PI)
    const theta = multiply(progress, 180)
    const rotate = interpolate(theta, {
        inputRange: [PI, 2 * PI],
        outputRange: [0, PI],
        extrapolate: Extrapolate.CLAMP
    });
    const opacity = lessThan(theta, PI)

    return (
        <Root>
            <CirclesContainer style={{ zIndex: 1 }}>
                <ETACircularProgressHalfCircle color={fg} size={size} />
                <Animated.View 
                    style={{ ...StyleSheet.absoluteFillObject,
                        transform: transformOrigin({x: 0, y: size / 2}, { rotate: theta }),
                        opacity
                    }}>
                    <ETACircularProgressHalfCircle color={bg} size={size} />
                </Animated.View>
            </CirclesContainer>

            <CirclesContainer style={{ transform: [{ rotate: '180deg' }] }}>
                <ETACircularProgressHalfCircle color={fg} size={size} />
                <Animated.View 
                    style={{ ...StyleSheet.absoluteFillObject,
                        transform: transformOrigin({x: 0, y: size}, { rotate }),
                    }}>
                    <ETACircularProgressHalfCircle color={bg} size={size} />
                </Animated.View>
            </CirclesContainer>
           
           
            <ViewStroked 
                style={{
                    zIndex: 2,
                    position: 'absolute',
                    left: (size / 3),
                    top: (size / 3),
                    // ...StyleSheet.absoluteFillObject,
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: size * 2 - (size / 3) * 2,
                    height: size * 2 - (size / 3) * 2,
                    borderRadius: size - (size / 3) / 2,
                    backgroundColor: fg
                }}/>
        </Root>
    );
})

export default ETACircularProgress;