import React, { useContext, memo } from 'react';
import styled, { ThemeContext } from 'styled-components/native';

const Root = styled.View`
`;
const HalfCircle = styled.View`

`; 

const ETACircularProgressHalfCircle = memo(({ color, size }) => {
    return (
        <Root style={{
            width: size * 2, 
            height: size,
            overflow: 'hidden'
        }}>
            <HalfCircle style={{ 
                backgroundColor: color, 
                width: size * 2,
                height: size * 2,
                borderRadius: size }}/>
        </Root>
    );
})

export default ETACircularProgressHalfCircle;