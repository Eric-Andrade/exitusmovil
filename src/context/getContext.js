import React from 'react';
import configAPI from '@context/configAPI';

const getReducer = (state, action) => {
    console.log('state', state);
    console.log('action', action.payload);

    switch(action.type) {
        case 'getBanks':
            return {
                data: action.payload.data,
                message: action.payload.message,
                error: action.payload.error,
                isLoading: false
            };
        case 'getCredits':
            return {
                data: action.payload.data,
                message: action.payload.message,
                error: action.payload.error,
                isLoading: false
            };
        case '_error':
            return {
                ...state,
                error: action.payload.error
            };
        default:
            return state;
    }
}

const getBanks = dispatch => async () => {
    try {
        let { data } = await configAPI.get('Bancos/GetBancos');
        // console.log('getBanks banks', data);
        
        const pay = {
            data: data,
            message: 'Banks gotten',
            error: ''
        }
        
        dispatch({
            type: 'getBanks',
            payload: pay
        })

    } catch (error) {
        console.log('getBanks: ', error);
        const pay = {
            data: '',
            message: '',
            error: error
        }
        dispatch({
            type: '_error',
            payload: pay
        })
    }
}

const getCredits = dispatch => async ({ PersonId }) => {
    try {
        let getCredits = await axios.get(`${URL2}api/Auth/Renovation?PersonId=${PersonId}`);
        let getCreditsRejected = await axios.get(`${URL2}api/Auth/RenovationRejected?PersonId=${PersonId}`);
        console.log('getCredits: ', getCredits.data);
        
        const pay = {
            data: 'data',
            message: 'Credits gotten',
            error: '',
        }

        dispatch({
            type: 'getCredits',
            payload: pay
        })

    } catch (error) {
        console.log('getCredits: ', error);
        
        const pay = {
            data: '',
            message: '',
            error: error
        }
        dispatch({
            type: '_error',
            payload: pay
        })
    }
}

export const { Provider, Context } = createContext(
    /* reducer */  getReducer,
   /* actions*/ { getBanks, getCredits },
   /* defaultValues */ { data: '', message: '', error: '', isLoading: true})