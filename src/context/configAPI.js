import axios from 'axios';
import { URL } from '@utils/constants';

const instance = axios.create({
    baseURL: URL,
    // timeout: 1000,
    // headers: {'X-Custom-Header': 'foobar'}
  });

export default instance;