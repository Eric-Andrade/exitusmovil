import React from 'react';
import axios from 'axios';
import { Alert } from 'react-native';
import createContext from '@context/createContext';
import AsyncStorage from '@react-native-community/async-storage';
import configAPI from '@context/configAPI';
import { URL2, URL3 } from '@utils/constants';
import { navigate } from '@commons/navbar/navigationRef';

const globalReducer = (state, action) => {
    // console.log('state', state);
    // console.log('action', action.payload);

    switch(action.type) {
        case 'restoreToken':
            return {
                ...state,
                data: action.payload.data,
                message: action.payload.message,
                error: action.payload.error,
                isLoading: false,
                userToken: action.payload.userToken
            };
        case 'logIn':
            return {
                ...state,
                data: action.payload.data,
                message: action.payload.message,
                error: action.payload.error,
                isLoading: false,
                userToken: action.payload.userToken
            };
        case 'signUp':
            return {
                data: action.payload.data,
                message: action.payload.message,
                error: action.payload.error,
                isLoading: false,
                userToken: action.payload.userToken
            };
        case 'logOut':
            return {
                data: action.payload.data,
                message: action.payload.message,
                error: action.payload.error,
                isLoading: false,
                userToken: action.payload.userToken
            };
        case 'verifyCode':
            return {
                data: action.payload.data,
                message: action.payload.message,
                error: action.payload.error,
                isLoading: false,
                userToken: action.payload.userToken
            };
        case 'renovatedCredits':
            return {
                data: action.payload.data,
                message: action.payload.message,
                error: action.payload.error,
                isLoading: false,
                userToken: action.payload.userToken
            };
        case 'dataRFC':
            return {
                data: action.payload.data,
                message: action.payload.message,
                error: action.payload.error,
                isLoading: false,
                userToken: action.payload.userToken
            };
        case 'updatePassword':
            return {
                data: action.payload.data,
                message: action.payload.message,
                error: action.payload.error,
                isLoading: false,
                userToken: action.payload.userToken
            };
        case 'operaRenovacion':
            return {
                data: action.payload.data,
                message: action.payload.message,
                error: action.payload.error,
                isLoading: false,
                userToken: action.payload.userToken
            };
        case '_error':
            return {
                ...state,
                error: action.payload.error
            };
        default:
            return state;
    }
}

const restoreToken = dispatch => async () => {
    let userToken;

    try {
        userToken = await AsyncStorage.getItem('@userToken');
        // console.log('restoreToken try', userToken);
    } catch (e) {
        // console.log('restoreToken catch');
    }

    const pay = {
        data: '',
        message: 'restoreToken message',
        error: '',
        userToken: userToken

    }

    dispatch({ 
        type: 'restoreToken',
        payload: pay 
    });
}

const logIn = dispatch => async ({ cellphone, password }) => {
    try {
        let { data } = await configAPI.get(`RegistroUsuario/Login?tel=${cellphone}&password=${password}`);
        console.log('[logIn] data: ', data);
        let userToken = null;

        if (!data.error) { // El RFC es clave en movimientos importantes si es diferente de null lo guardaremos en el Asynctorage 
            if (data.nipval) { // Validar si la cuenta está validada
                if (!data.logged) { // Si sí está validada, verificar que no esté abierta la sesión en otra parte (solo se permite una sesión abierta)
                    userToken = data.rfc
                    await AsyncStorage.setItem('@userDataFullName', `${data.nombre} ${data.paterno} ${data.materno}`)
                    await AsyncStorage.setItem('@userTelefono', `${data.telefono}`)
                    let personIDTemp = 0
                    // let pId = await GetPersonId({personIDTemp: personIDTemp, nombre: 'MARIA DEL CARMEN', snombre: '', paterno: 'RODRIGUEZ', materno: 'DIONISIO', BirthDate: '19730720', rfc: ''})
                    let pId = await GetPersonId({personIDTemp: personIDTemp, nombre: data.nombre, snombre: data.snombre, paterno: data.paterno, materno: data.materno, BirthDate: '', rfc: data.rfc})
                    console.log('[GetPersonId - logIn] personId: ', pId.PersonId);
                    
                    await AsyncStorage.setItem('@PersonId', pId.PersonId)
                    await AsyncStorage.setItem('@userToken', userToken)                 
                    const pay = {
                        data: data,
                        message: 'User logged',
                        error: data.logged,
                        userToken: userToken,
                    }
                    
                    dispatch({
                        type: 'logIn',
                        payload: pay
                    })
                } else {
                    console.log('[else] logged:'), data.logged;
                    Alert.alert('No se ha podido ingresar','Su cuenta está abierta en otro dispositivo, favor de cerrarla antes de continuar por medio de esta aplicación.',
                    [
                        { text: 'De acuerdo', onPress: () => console.log('OK Pressed') }
                    ])
                }
            } else {
                await console.log('Registro no completado, dirigido a verificar NIP', data.rfc);
                // close pre-login
                let logOutdata = await configAPI.get(`RegistroUsuario/CierraSesion?rfc=${data.rfc}`);
                await console.log('[logOut] data: ', logOutdata.data)
                await navigate('PreValidationScreen', { propsrfc: data.rfc, propop: 1, propcellphone: cellphone })
            }
        } else {
            const pay = {
                data: data,
                message: 'User logged faild',
                error: data.logged,
                userToken: null,
            }
            
            dispatch({
                type: 'logIn',
                payload: pay
            })
            // Alert.alert('Por favor, corrobore sus datos e inténtelo de nuevo.', '', [
            //     { text: 'De acuerdo', onPress: () => console.log('OK Pressed') }
            // ])
        }
        
    } catch (error) {
        console.log('[logIn] error: ', error);
        
        const pay = {
            data: '',
            message: '',
            error: error
        }

        dispatch({
            type: '_error',
            payload: pay
        })
    }
}

const signUp = dispatch => async ({ name, secondname, lastname, secondlastname, birthdate, email, password, confirmpassword, cellphone, clabeult, bank, privacy }) => {
    try {
        let newDate = await _onChangeDate(birthdate)
        // console.log('signUp: ', { name, secondname, lastname, secondlastname, newDate, email, password, confirmpassword, cellphone, clabeult, bank, privacy });
        console.log('signUp: ', { name, secondname, lastname, secondlastname, ApFNacimiento: newDate, email, password, cellphone, clabeult, bank, privacy });
        let { data } = await configAPI.get(`RegistroUsuario/RegistraUsuario?ApPNombre=${name}&ApSNombre=${secondname}&ApAPaterno=${lastname}&ApAMaterno=${secondlastname}&ApFNacimiento=${newDate}&ApBanco=${bank}&ApClabe=${clabeult}&ApEmail=${email}&ApTelefono=${cellphone}&ApContrasena=${password}&Correo=${email}`);
                                        // RegistroUsuario/RegistraUsuario?ApPNombre=TEST&ApSNombre=TEST&ApAPaterno=TEST&ApAMaterno=TEST&ApFNacimiento=2002-12-17&ApBanco=Banorte&ApClabe=1234&ApEmail=TEST@TEST.COM&ApTelefono=5620222699&ApContrasena=TEST&Correo=TEST@TEST.COM

        console.log('[signUp] data:', data);
        if (data.rfc) {
            let newDateWLash = await _onChangeDateWLash(birthdate)
            let pIdsignUp = await GetPersonId({personIDTemp: 0, nombre: name, snombre: secondname, paterno: lastname, materno: secondlastname, BirthDate: newDateWLash, rfc: data.rfc})
            console.log('[GetPersonId - signUp] personId: ', pIdsignUp);
            if (pIdsignUp.ErrorCode === '0') { // si no hay error / se encontró
                RegistraPersonId(pIdsignUp.PersonId, data.rfc)
            }
            if (!data.error) {
                console.log('dirigido a CodeConfirmationScreen');
                /* VerificaCodigo
                    op = 1: Nuevo registro
                    op = 2: Recuperar contraseña
                */
                navigate('CodeConfirmationScreen', { nextScreen: 'LoginScreen', propsrfc: data.rfc, propop: 1, propcellphone: cellphone, propemail: email })
            }
        }
        
        const pay = {
            data: data,
            message: data.respuesta,
            error: data.error,
            userToken: null
        }
        
        dispatch({
            type: 'signUp',
            payload: pay
        })
    } catch (error) {
        console.log('signUp error: ', error);
        
        const pay = {
            data: '',
            message: '',
            error: error
        }

        dispatch({
            type: '_error',
            payload: pay
        })
    }
}

const _onChangeDate = (selectedDate) => {
    let yearconverted = selectedDate.getFullYear();
    let monthconverted = ('0' + (selectedDate.getMonth() + 1)).slice(-2)
    let dayconverted = ('0' + (Platform.OS == 'ios' ? selectedDate.getDate() + 1 : selectedDate.getDate())).slice(-2)
    var _completeDate = `${yearconverted}-${monthconverted}-${dayconverted}`;

    return _completeDate;
};

const _onChangeDateWLash = (selectedDate) => {
    let yearconverted = selectedDate.getFullYear();
    let monthconverted = ('0' + (selectedDate.getMonth() + 1)).slice(-2)
    let dayconverted = ('0' + (Platform.OS == 'ios' ? selectedDate.getDate() + 1 : selectedDate.getDate())).slice(-2)
    var _completeDate = `${yearconverted}${monthconverted}${dayconverted}`;

    return _completeDate;
};

const GetPersonId = async ({ personIDTemp, nombre, snombre, paterno, materno, BirthDate, rfc }) => {
    let datapersonID = await axios.get(`${URL2}api/Auth/Identify?PersonId=${personIDTemp}&FirstName=${nombre}&SecondName=${snombre}&FLastName=${paterno}&SLastName=${materno}&BirthDate=${BirthDate}&TreasuryId=${rfc}&LegalId=&L4Digits=`);
    // console.log('[GetPersonId] data',  datapersonID.data);
    let personId = JSON.parse(datapersonID.data)

    return personId
}

const logOut = dispatch => async ({ rfc }) => {
    console.log('[logOut]: ', rfc);
    try {
        let { data } = await configAPI.get(`RegistroUsuario/CierraSesion?rfc=${rfc}`);

        console.log('[logOut] data: ', data);
        await AsyncStorage.removeItem('@userToken');
        await AsyncStorage.removeItem('@PersonId');
        await AsyncStorage.removeItem('@userTelefono');
        
        const pay = {
            data: '',
            message: 'User logged out',
            error: '',
            userToken: null
        }
        
        dispatch({
            type: 'logOut',
            payload: pay
        })

    } catch (error) {
        console.log('logOut error: ', error);
        
        const pay = {
            data: '',
            message: '',
            error: error
        }

        dispatch({
            type: '_error',
            payload: pay
        })
    }
}

const verifyCode = dispatch => async ({ rfc, code, op, nextScreen, propcellphone }) => {
    console.log('context verifyCode: ', { rfc, code, op, nextScreen });
    try {
        let { data } = await configAPI.get(`RegistroUsuario/VerificaCodigo?rfc=${rfc}&codigo=${code}&op=${op}`);
        console.log('[verifyCode] data:', data);
        
        /* VerificaCodigo
            op = 1: Nuevo registro
            op = 2: Recuperar contraseña
        */

        if (!data.error && op === 1) {
            // userToken = data.rfc
            // await AsyncStorage.setItem('@userToken', userToken)
            console.log('cierra sesión y va a login', rfc);
            logOut({ rfc: rfc })
            setTimeout(() => {
                navigate(nextScreen) // go to login screen
            }, 2500);
        } else if (!data.error && op === 2) {
            navigate(nextScreen, { propcellphone }) // go to update screen
        } else {
            console.log('else');
        }

        const pay = {
            data: data,
            message: data.respuesta,
            error: data.error,
            userToken: null,
            isLoading: data.isLoading
        }

        dispatch({
            type: 'verifyCode',
            payload: pay
        })

    } catch (error) {
        console.log('verifyCode error: ', error);
        
        const pay = {
            data: '',
            message: '',
            error: error
        }

        dispatch({
            type: '_error',
            payload: pay
        })
    }
}

const renovatedCredits = dispatch => async ({ CreditId }) => {
    try {
        const PersonId = await AsyncStorage.getItem('@PersonId')
        console.log('renovatedCredits', {CreditId, PersonId});
        let { data } = await configAPI.get(`RegistroUsuario/RegistraCreditosRenovados?personId=${PersonId}&CreditId=${CreditId}`);
        
        const pay = {
            data: data,
            message: 'User credits log',
            error: '',
            // userToken: null
        }

        dispatch({
            type: 'renovatedCredits',
            payload: pay
        })

    } catch (error) {
        console.log('renovatedCredits error: ', error);
        
        const pay = {
            data: '',
            message: '',
            error: error
        }

        dispatch({
            type: '_error',
            payload: pay
        })
    }
}

const dataRFC = dispatch => async ({ rfc }) => {
    try {
        console.log('dataRFC: ', rfc);
        let { data } = await configAPI.get(`RegistroUsuario/CargaDatosRFC?rfc=${rfc}`);
        
        const pay = {
            data: data,
            message: 'User rfc',
            error: '',
            // userToken: null
        }

        dispatch({
            type: 'dataRFC',
            payload: pay
        })

    } catch (error) {
        console.log('dataRFC error: ', error);
        
        const pay = {
            data: '',
            message: '',
            error: error
        }

        dispatch({
            type: '_error',
            payload: pay
        })
    }
}

const updatePassword = dispatch => async ({ cellphone, email, pass, op, screen }) => {
    console.log('[Context] updatePassword: ', { cellphone, email, pass, op, screen });
    /*
    op = 1: consulta similar a op 2
    op = 2: buscar teléfono?
    op = 3: actualiza pass
    */
   try {
        let { data } = await configAPI.get(`RegistroUsuario/UpdatePassword?numero=${cellphone}&pass=${pass}&op=${op}&Correo=${email}`);
        console.log('[Context] cellphone: ', {cellphone, pass, op});

        if (!data.error) {
            /* VerificaCodigo
                op = 1: Nuevo registro
                op = 2: Recuperar contraseña
            */
            // console.log('updatePassword cellphone: ', {cellphone, pass, op});
            navigate(screen, { nextScreen: 'UpdatePasswordScreen', propsrfc: data.rfc, propop: 2, propcellphone: cellphone, propemail: email })
        }

        const pay = {
            data: data,
            message: data.respuesta,
            error: data.error,
            userToken: null
        }

        dispatch({
            type: 'updatePassword',
            payload: pay
        })

    } catch (error) {
        console.log('updatePassword error: ', error);
        
        const pay = {
            data: '',
            message: '',
            error: error
        }

        dispatch({
            type: '_error',
            payload: pay
        })
    }
}

////////////////////////////////// POST //////////////////////////////////////

const operaRenovacion = dispatch => async ({ CreditId }) => {
    console.log('[Context] operaRenovacion', {CreditId});
    try {
        let { data } = await axios.get(URL2 + `api/Auth/OperaRenovacion?CreditId=${CreditId}&appId=APP`);
        console.log('operaRenovacion: ', data);

        if (data) {
            navigate('HomeScreen')
        }
        
        const pay = {
            // data: 'data',
            data: data,
            message: 'operaRenovacion',
            error: '',
            // userToken: null
        }
        
        dispatch({
            type: 'operaRenovacion',
            payload: pay
        })
        
    } catch (error) {
        console.log('operaRenovacion error: ', error);
        
        const pay = {
            data: '',
            message: '',
            error: error
        }

        dispatch({
            type: '_error',
            payload: pay
        })
    }
}

const RegistraPersonId = async (PersonId, rfc) => {
    try {
        // const userToken = await AsyncStorage.getItem('@userToken');
        // const PersonId = await AsyncStorage.getItem('@PersonId');
        console.log('PersonId, rfc: ', PersonId, rfc);
        if (rfc && PersonId) {
            let { data } = await configAPI.get(`RegistraPersonId?PersonId=${PersonId}&rfc=${rfc}`);
            console.log('RegistraPersonId: ', data);
            
            const pay = {
                data: data,
                message: 'RegistraPersonId',
                error: '',
                // userToken: null
            }
            
            dispatch({
                type: 'RegistraPersonId',
                payload: pay
            })
        }
    } catch (error) {
        console.log('RegistraPersonId error: ', error);
        
        const pay = {
            data: '',
            message: '',
            error: error
        }

        dispatch({
            type: '_error',
            payload: pay
        })
    }
}

export const { Provider, Context } = createContext(
     /* reducer */ globalReducer,
    /* actions*/ { restoreToken, logIn, signUp, logOut, verifyCode,
                    renovatedCredits, updatePassword, operaRenovacion, dataRFC, RegistraPersonId },
    /* defaultValues */ { data: '', message: '', error: '', isLoading: true, userToken: null }
)