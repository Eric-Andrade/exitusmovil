import React, { useState, useEffect, useLayoutEffect } from 'react';
import styled from 'styled-components/native';
import AsyncStorage from '@react-native-community/async-storage';
import ProfileForm from '@components/ProfileForm';

const Root = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;

const ProfileScreen = () => {
    const [ userData, setuserData ] = useState('')

    useEffect(() => {
        let isUnmount = false
        const getData = async () => {
            getuserData =  await AsyncStorage.getItem('@userDataFullName');
            setuserData(`${getuserData}`)
        }
        getData()
        return () => {
          isUnmount = true
        }
    }, [])

    return (
        <Root>
            <ProfileForm />
        </Root>  
      );
}

export default ProfileScreen;