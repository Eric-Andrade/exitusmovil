import React from 'react';
import styled from 'styled-components/native';
import RenovationCreditMessage from '@components/RenovationCreditMessage';

const Root = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
    backgroundColor: ${props => props.theme.PRIMARY_TEXT_BACKGROUND_COLOR}
`;

const RenovationCreditScreen = () => {
    return (
        <Root>
            <RenovationCreditMessage />
        </Root>  
      );
}

export default RenovationCreditScreen;