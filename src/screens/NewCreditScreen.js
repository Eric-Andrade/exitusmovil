import React from 'react';
import styled from 'styled-components/native';
import NewCredit from '@components/NewCredit';

const Root = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;

const NoticeOfPrivacyScreen = () => {
    return (
        <Root>
            <NewCredit />
        </Root>  
      );
}

export default NoticeOfPrivacyScreen;