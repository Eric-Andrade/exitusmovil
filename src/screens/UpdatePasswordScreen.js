import React from 'react';
import styled from 'styled-components/native';
import UpdatePasswordComponent from '@components/RecoveryPassForm/UpdatePasswordComponent';

const Root = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;

const UpdatePasswordScreen = () => {
    return (
        <Root>
            <UpdatePasswordComponent />
        </Root>  
    );
}

export default UpdatePasswordScreen;