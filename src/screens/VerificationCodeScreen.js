import React from 'react';
import styled from 'styled-components/native';
import VerificationCodeComponent from '@components/RecoveryPassForm/VerificationCodeComponent';

const Root = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;

const VerificationCodeScreen = () => {
    return (
        <Root>
            <VerificationCodeComponent />
        </Root>  
    );
}

export default VerificationCodeScreen;