import React from 'react';
import styled from 'styled-components/native';
import NoticeOfPrivacyMessage from '@components/NoticeOfPrivacyMessage';

const Root = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;

const NoticeOfPrivacyScreen = () => {
    return (
        <Root>
            <NoticeOfPrivacyMessage />
        </Root>  
      );
}

export default NoticeOfPrivacyScreen;