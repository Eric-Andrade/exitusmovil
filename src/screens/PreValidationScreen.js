import React from 'react';
import styled from 'styled-components/native';
import PreValidation from '@components/PreValidation';

const Root = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;

const PreValidationScreen = () => {
    return (
        <Root>
            <PreValidation />
        </Root>  
      );
}

export default PreValidationScreen;