import React, { useState, useEffect, useLayoutEffect } from 'react';
import styled from 'styled-components/native';
import AsyncStorage from '@react-native-community/async-storage';
import HomeList from '@components/Home/HomeList';
import HomeHead from '@components/Home/HomeHead';

const Root = styled.View`
    flex: 1;
`;

const HomeScreen = ({ navigation, route }) => {
    const [ userData, setuserData ] = useState()

    useEffect(() => {
        let isUnmount = false
        const getData = async () => {
            getuserData =  await AsyncStorage.getItem('@userDataFullName');
            await setuserData(getuserData)
        }
        
        getData()
        return () => {
          isUnmount = true
        }
    }, [])
    
    useLayoutEffect(() => {
        navigation.setOptions({ headerTitle: `Exitus Renova` });

    }, [ navigation, route ]);
    
    return (
        <Root>
            {
                userData
                ?   <>
                        <HomeHead userData={userData}/>
                    </>
                :   null
            }
            <HomeList />
        </Root>  
      );
}

export default HomeScreen;