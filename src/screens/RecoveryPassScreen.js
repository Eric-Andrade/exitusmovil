import React from 'react';
import styled from 'styled-components/native';
import RecoveryPassForm from '@components/RecoveryPassForm';

const Root = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;

const RecoveryPassScreen = () => {
    return (
        <Root>
            <RecoveryPassForm />
        </Root>  
      );
}

export default RecoveryPassScreen;