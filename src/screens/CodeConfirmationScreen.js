import React from 'react';
import styled from 'styled-components/native';
import CodeConfirmationForm from '@components/SignUp/CodeConfirmationForm';

const Root = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;

const CodeConfirmationScreen = () => {
    return (
        <Root>
            <CodeConfirmationForm />
        </Root>  
      );
}

export default CodeConfirmationScreen;