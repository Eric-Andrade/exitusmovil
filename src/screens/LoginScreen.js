import React from 'react';
import { TouchableWithoutFeedback, Keyboard, KeyboardAvoidingView } from 'react-native';
import LoginHead from '@components/Login/LoginHead';
import LoginForm from '@components/Login/LoginForm';
import LoginBody from '@components/Login/LoginBody'; 
import styled from 'styled-components/native';

const Root = styled.View`
    flex: 1;
    alignItems: center;
    justify-content: flex-start;
`;

const LoginScreen = () => {
    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()} style={{ flex: 1 }}>
            <Root>
                {/* <KeyboardAvoidingView behavior='height'> */}
                    <LoginHead />
                    <LoginForm />
                    {/* <LoginBody /> */}
                {/* </KeyboardAvoidingView> */}
            </Root>
        </TouchableWithoutFeedback>
    );
}

export default LoginScreen;