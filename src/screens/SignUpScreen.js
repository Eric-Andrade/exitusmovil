import React from 'react';
import { TouchableWithoutFeedback, Keyboard, ScrollView } from 'react-native';
import styled from 'styled-components/native';
import SignUpForm from '@components/SignUp/SignUpForm';

const Root = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;

const SignUpScreen = () => {

    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Root>
                    <SignUpForm />
                </Root>
            </ScrollView>
        </TouchableWithoutFeedback>
    );
}

export default SignUpScreen;