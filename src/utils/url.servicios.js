export const URL_REGISTRO = 'http://exitusapp-2.eastus2.cloudapp.azure.com/RenovaApp/RegistroUsuario/RegistraUsuario'
export const URL_LOGIN = 'http://exitusapp-2.eastus2.cloudapp.azure.com/RenovaApp/RegistroUsuario/Login'
export const URL_VERIFICACODIGO = 'http://exitusapp-2.eastus2.cloudapp.azure.com/RenovaApp/RegistroUsuario/VerificaCodigo'
export const URL_UPDATEPASS = 'http://exitusapp-2.eastus2.cloudapp.azure.com/RenovaApp/RegistroUsuario/UpdatePassword'
export const URL_BANCO = 'http://exitusapp-2.eastus2.cloudapp.azure.com/RenovaApp/Bancos/GetBancos';
export const URL_CREDITOSRECHAZADOS = 'http://exitusapp-2.eastus2.cloudapp.azure.com/ExitusApiZPR/api/Auth/RenovationRejected'
export const URL_CREDITOS = 'http://exitusapp-2.eastus2.cloudapp.azure.com/ExitusApiZPR/api/Auth/Renovation'
export const URL_CS = 'http://exitusapp-2.eastus2.cloudapp.azure.com/RenovaApp/RegistroUsuario/CierraSesion'
export const URL_DATOSRFC = 'http://exitusapp-2.eastus2.cloudapp.azure.com/RenovaApp/RegistroUsuario/CargaDatosRFC'
export const URL_RENOVA = 'http://exitusapp-2.eastus2.cloudapp.azure.com/ExitusApiZPR/api/Auth/OperaRenovacion'
export const URL_PERSONID = 'http://exitusapp-2.eastus2.cloudapp.azure.com/ExitusApiZPR/api/Auth/Identify?PersonId=&';
export const URL_RCR = 'http://exitusapp-2.eastus2.cloudapp.azure.com/RenovaApp/RegistroUsuario/RegistraCreditosRenovados'



//RegistraPersonId
export const URL_RP = 'http://exitusapp-2.eastus2.cloudapp.azure.com/RenovaApp/RegistroUsuario/RegistraPersonId'
//RegistaCreditosRenovados
//UPDATE PASS

//Veirifca la version de la aplicacion
export const VAPP = 'http://exitusapp-2.eastus2.cloudapp.azure.com/Controladores/verificaVersion?';
//Version App
export const vAndroid = '2.0.0';
//Version iOS
// ./code/CRN/exitusmovil/src/**/*.js
// ./code/renova_v1/src/**/*.ts