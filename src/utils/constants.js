export const URL = 'https://www.exituscloud.com/RenovaApp_TEST/'
export const URL2 = 'https://www.exituscloud.com/ExitusApiZ/' // TEST: https://www.exituscloud.com/ExitusApiZ
export const URL3 = 'https://www.exituscloud.com/Controladores_TEST/'
export const URL4 = 'https://www.exituscloud.com/CredibotLog_Test/log'
export const vAndroid = '1.5.4';
export const vIOS = '1.0.0';

// Id    VersionApp    Descripcion
// 1    1.5.1         APP Renova
// 2    1.7.11        APP Nuevos
// 3    1.5.4         APP RENOVA ANDROID

const SLUG = 'exitusmovil'

export const variables = {
  COMPANYNAME: 'Exitus Credit',
  COMPANYSLOGAN: 'Exitus Credit slogan',
  COMPANYURL: `https://www.${SLUG}.com`,
  COMPANYMAIL: `help@${SLUG}.com`,
  AVATAR_USER_DEFAULT: 'https://www.researchgate.net/profile/Maria_Monreal2/publication/315108532/figure/fig1/AS:472492935520261@1489662502634/Figura-2-Avatar-que-aparece-por-defecto-en-Facebook.png'
}

export const base = {
  FONT_SIZE_TINY: 8,
  FONT_SIZE_SEMISMALL: 10,
  FONT_SIZE_SMALL: 12,
  FONT_SIZE_SEMIMEDIUM: 13,
  FONT_SIZE_MEDIUM: 14,
  FONT_SIZE_SEMILARGE: 16,
  FONT_SIZE_LARGE: 18,
  FONT_SIZE_EXTRA_LARGE: 24,
  FONT_SIZE_SEMIMASSIVE: 28,
  FONT_SIZE_MASSIVE: 34,

  FONT_WEIGHT_LIGHT: '200',
  FONT_WEIGHT_SEMIMEDIUM: '400',
  FONT_WEIGHT_MEDIUM: '500',
  FONT_WEIGHT_BOLD: '700',

  PRIMARY_FONT_FAMILY: 'AvertaDemo-Regular',
  PRIMARY_FONT_FAMILY_BOLD: 'AvertaDemo-ExtraBoldItalic',

  SECONDARY_FONT_FAMILY: 'Product-Sans-Regular',
  SECONDARY_FONT_FAMILY_ITALIC: 'Product-Sans-Italic',
  
  SHADOW: 'rgba(153, 153, 153, 0.3)',
  TRANSPARENT: 'rgba(0, 0, 0, 0)',
  WHITE: 'rgba(255, 255, 255, 1)',
  WHITE10: 'rgba(255, 255, 255, 0.10)',
  WHITE25: 'rgba(255, 255, 255, 0.25)',
  WHITE50: 'rgba(255, 255, 255, 0.50)',
  WHITE75: 'rgba(255, 255, 255, 0.75)',
  WHITE85: 'rgba(255, 255, 255, 0.85)',
  WHITE90: 'rgba(255, 255, 255, 0.90)',
  BLACK20: 'rgba(0, 0, 0, 0.2)',
  BLACK30: 'rgba(0, 0, 0, 0.3)',
  BLACK50: 'rgba(0, 0, 0, 0.5)',
  BLACK: 'rgba(0, 0, 0, 1)',
  GRAYSEMILIGHT: 'rgba(217, 217, 217, 1)',
  GRAYLIGHT: 'rgba(242, 243, 245, 1)',
  GRAYFBINPUTTEXT: 'rgba(141, 148, 158, 1)',
  YELLOW: 'rgba(246, 173, 0, 1)',
  RED: 'rgba(255, 59, 48, 1)',
  GREEN: '#4CD964',

  noImage : 'https://www.brandenforcement.co.uk/wp-content/themes/brand-enforce/img/default-placeholder.png'
};

export const darkTheme = {
  PRIMARY_COLOR: '#404040',
  PRIMARY_BACKGROUND_COLOR: '#3D3D3D',
  PRIMARY_BACKGROUND_COLOR_LIGHT: '#797979',

  EXITUS_COLOR: '#E62F41',

  SECONDARY_COLOR: '#EA1F43',
  SECONDARY_BACKGROUND_COLOR: '#FFFFFF',
  SECONDARY_BACKGROUND_COLOR_LIGHT: '#F7F7F7',

  THIRD_BACKGROUND_COLOR: '#A6A6A6',
  THIRD_BACKGROUND_COLOR_LIGHT: '#E6E6E6',

  FOURTH_BACKGROUND_COLOR: '#EFEFF4',

  REJECTED_COLOR: '#B30000',
  SUCCESS_COLOR: '#009900',
  FAIL_COLOR: '#ff3333',
  GRAYFACEBOOK: '#777777',

  BACKGROUND_COLOR: '#333333',

  PRIMARY_TEXT_COLOR: '#FFFFFF',
  PRIMARY_TEXT_COLOR_LIGHT: '#BBBBBB',
  SECONDARY_TEXT_COLOR: '#3D3D3D',
  SECONDARY_TEXT_COLOR_LIGHT: 'rgba(90, 90, 90, 1)',
  THIRD_TEXT_COLOR: '#444',
  THIRD_TEXT_COLOR_LIGHT: 'rgba(169, 169, 169, 1)',

  PRIMARY_TEXT_BACKGROUND_COLOR: '#3D3D3D',
  SECONDARY_TEXT_BACKGROUND_COLOR: '#FFFFFF'
};

export const lightTheme = {
  PRIMARY_COLOR: '#EA1F43',
  // PRIMARY_COLOR: '#D32345',
  PRIMARY_BACKGROUND_COLOR: '#E6E6E6',
  PRIMARY_BACKGROUND_COLOR_LIGHT: '#f7f7f7',
  EXITUS_COLOR: '#EA1F43',
  
  SECONDARY_COLOR: '#404040',
  SECONDARY_BACKGROUND_COLOR: '#3D3D3D',
  SECONDARY_BACKGROUND_COLOR_LIGHT: '#797979',

  THIRD_BACKGROUND_COLOR: '#D9D9D9',
  THIRD_BACKGROUND_COLOR_LIGHT: '#FFFFFF',

  FOURTH_BACKGROUND_COLOR: '#EFEFF4',

  REJECTED_COLOR: '#ff6666',
  SUCCESS_COLOR: '#61ca72',
  FAIL_COLOR: '#ff3333',
  GRAYFACEBOOK: '#777777',

  BACKGROUND_COLOR: 'rgba(248, 248, 248, 0.82)',

  PRIMARY_TEXT_COLOR: '#333333',
  PRIMARY_TEXT_COLOR_LIGHT: '#595959',
  SECONDARY_TEXT_COLOR: '#FFFFFF',
  SECONDARY_TEXT_COLOR_LIGHT: 'rgba(142, 142, 147, 1)',
  THIRD_TEXT_COLOR: '#f7f7f7',
  THIRD_TEXT_COLOR_LIGHT: 'rgba(169, 169, 169, 1)',

  PRIMARY_TEXT_BACKGROUND_COLOR: '#FFFFFF',
  SECONDARY_TEXT_BACKGROUND_COLOR: '#3D3D3D'
};

export const colorOptions = {
  orange: {
    PRIMARY_COLOR_FAINT: '#FFF3E0',
    PRIMARY_COLOR_LIGHT: '#FFB74D',
    PRIMARY_COLOR: '#FF9800',
    PRIMARY_COLOR_BOLD: '#EF6C00',
    PRIMARY_FOREGROUND_COLOR: '#FFFFFF'
  },
  red: {
    PRIMARY_COLOR_FAINT: '#FFEBEE',
    PRIMARY_COLOR_LIGHT: '#E57373',
    PRIMARY_COLOR: '#F44336',
    PRIMARY_COLOR_BOLD: '#C62828',
    PRIMARY_FOREGROUND_COLOR: '#FFFFFF'
  },
  blue: {
    PRIMARY_COLOR_FAINT: '#E3F2FD',
    PRIMARY_COLOR_LIGHT: '#64B5F6',
    PRIMARY_COLOR: '#2196F3',
    PRIMARY_COLOR_BOLD: '#1565C0',
    PRIMARY_FOREGROUND_COLOR: '#FFFFFF'
  },
  cyan: {
    PRIMARY_COLOR_FAINT: '#E0F7FA',
    PRIMARY_COLOR_LIGHT: '#4DD0E1',
    PRIMARY_COLOR: '#00BCD4',
    PRIMARY_COLOR_BOLD: '#00838F',
    PRIMARY_FOREGROUND_COLOR: '#FFFFFF'
  },
  teal: {
    PRIMARY_COLOR_FAINT: '#E0F2F1',
    PRIMARY_COLOR_LIGHT: '#4DB6AC',
    PRIMARY_COLOR: '#009688',
    PRIMARY_COLOR_BOLD: '#00695C',
    PRIMARY_FOREGROUND_COLOR: '#FFFFFF'
  },
  gray: {
    PRIMARY_COLOR_FAINT: '#FAFAFA',
    PRIMARY_COLOR_LIGHT: '#E0E0E0',
    PRIMARY_COLOR: '#9E9E9E',
    PRIMARY_COLOR_BOLD: '#424242',
    PRIMARY_FOREGROUND_COLOR: '#FFFFFF'
  },
  purlple: {
    PRIMARY_COLOR_FAINT: '#EDE7F6',
    PRIMARY_COLOR_LIGHT: '#9575CD',
    PRIMARY_COLOR: '#673AB7',
    PRIMARY_COLOR_BOLD: '#4527A0',
    PRIMARY_FOREGROUND_COLOR: '#FFFFFF'
  },
  green: {
    PRIMARY_COLOR_FAINT: '#E8F5E9',
    PRIMARY_COLOR_LIGHT: '#81C784',
    PRIMARY_COLOR: '#4CAF50',
    PRIMARY_COLOR_BOLD: '#2E7D32',
    PRIMARY_FOREGROUND_COLOR: '#FFFFFF'
  },
  pink: {
    PRIMARY_COLOR_FAINT: '#F5E8F2',
    PRIMARY_COLOR_LIGHT: '#C781C2',
    PRIMARY_COLOR: '#FF2B4F',
    PRIMARY_COLOR_BOLD: '#7D2E5D',
    PRIMARY_FOREGROUND_COLOR: '#FFFFFF'
  },
  white: {
    PRIMARY_COLOR_FAINT: '#FFFFFF',
    PRIMARY_COLOR_LIGHT: 'rgba(255, 255, 255, 0.7)',
    PRIMARY_COLOR: '#FF2B4F',
    PRIMARY_COLOR_BOLD: '#7D2E5D',
    PRIMARY_FOREGROUND_COLOR: '#FFFFFF'
  }
};

export const mycolorSchema = {
  dark: false,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: '#F1F1F1',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(199, 199, 204)',
  },
};

export const mycolorSchemaDark = {
  dark: false,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: '#262626',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(242, 242, 242)',
    border: 'rgb(199, 199, 204)',
  },
};