import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/native';
import { Platform, Dimensions, useColorScheme } from 'react-native';
import { ETASimpleText, ETALink } from '@etaui';

const { width } = Dimensions.get('window')

const Root = styled.View`
    flex: 1;
    width: ${width - 50}px;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
    alignSelf: center;
    alignContent: center;
    marginVertical: 10px;
`;
const NOPContainer = styled.View`
    min-height: 350px;
    justifyContent: center;
    alignItems: center;
    padding-horizontal: 20px;
    padding-vertical: 20px;
    border-radius: 10px;
    background-color: white;
`;
const HeaderLogo = styled.Image`
    margin-bottom: 30px
`;

const NoticeOfPrivacyMessage = () => {
    const themeContext = useContext(ThemeContext);
    const colorSchema = useColorScheme();

    return (
        <>
            <Root>
                <HeaderLogo 
                    style={{
                        height: Platform.OS === 'ios' ? 30 : 30,
                        width: Platform.OS === 'ios' ? 140 : 140 }}
                    source={colorSchema == 'dark' ? require('@assets/icons/app-icon.png') : require('@assets/icons/app-icon.png')}
                />
                <NOPContainer>
                    <ETASimpleText size={15} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'justify'}>
                        En este sentido, y de conformidad con lo que establece el artículo 8º de la LFPDPPP, requerimos de su consentimiento expreso para el trámite de sus datos personales, financieros y patrimoniales, por lo que le solicitamos que indique si acepta o no el tratamiento, en el entendido que de no hacerlo no podrá aplicar su solicitud para el crédito solicitado. Más información, visite {' '}
                        <ETALink url='https://www.exituscredit.com' text='https://www.exituscredit.com' size={15} weight='700' color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'justify'} />, en la sección Aviso de Privacidad.
                    </ETASimpleText>
                </NOPContainer>
            </Root>
        </>
    );
}

export default NoticeOfPrivacyMessage;