import React, {  useState, useEffect, useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/native';
import { Platform, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Formik } from 'formik';
import * as yup from 'yup';
import { ETAButtonFilled, ETATextInputFilled, ETASimpleText } from '@etaui';
import { Context } from '@context';
import ProfileHeadComponent from './ProfileHeadComponent';

const Root = styled.View`
    flex: 1;
    width: 100%;
    justifyContent: center;
    alignItems: center;
`;
const FormContainer = styled.View`
    flex: 0.8;
    width: 100%;
    display: flex;
    flexDirection: column;
    justifyContent: flex-start;
    alignItems: center;
    margin-top: 20px;
    background-color: transparent;
`;
const ColumnContainer = styled.View`
    display: flex;
    flexDirection: column;
`;
const TitleInputContainer = styled.View`
    width: 100%;
    display: flex;
    margin-top: 3px;
    justifyContent: flex-start;
    alignItems: flex-start;
    background-color: transparent;
`;

const validationSchema = yup.object().shape({
    name: yup
        .string()
        .matches(/^[A-Za-z ]*$/, 'Únicamente ingrese letras sin tilde')
        .required('Este campo no puede estar vacío')
        .uppercase(),
    secondname: yup
        .string()
        .matches(/^[A-Za-z ]*$/, 'Únicamente ingrese letras sin tilde')
        .uppercase(),
    lastname: yup
        .string()
        .matches(/^[A-Za-z ]*$/, 'Únicamente ingrese letras sin tilde')
        .required('Este campo no puede estar vacío')
        .uppercase(),
    secondlastname: yup
        .string()
        .matches(/^[A-Za-z ]*$/, 'Únicamente ingrese letras sin tilde')
        .required('Este campo no puede estar vacío')
        .uppercase(),
    cellphone: yup
        .string()
        .matches(/^[0-9]*$/, 'Ingrese únicamente números')
        .min(10, 'Teléfono celular debe tener 10 dígitos')
        .max(10, 'Teléfono celular debe tener 10 dígitos')
        .required('Este campo no puede estar vacío')
        .typeError('Ingrese únicamente números'),
    rfc: yup
        .string()
        .matches(/^[A-Za-z0-9]*$/, 'Únicamente ingrese letras sin tilde y números')
        .required('Este campo no puede estar vacío')
        .uppercase(),
});

const ProfileForm = () => {
    const { logOut, dataRFC, state } = useContext(Context);
    const themeContext = useContext(ThemeContext);
    const [ token, settoken ] = useState()

    useEffect(() => {
        let isUnmount = false
        _getProfile()
        return () => {
          isUnmount = true
        }
    }, [])

    const _getProfile = async () => {
        try {
            const userToken = await AsyncStorage.getItem('@userToken')
            // console.log('profile: ', userToken);
            
            settoken(userToken);
            if (userToken) {
                dataRFC({ rfc: userToken })
                // console.log('_getProfile state', state)
            }
        // try {
        //     const userToken = await AsyncStorage.getItem('@userToken')
        //     settoken(userToken);
        //     if (token) {
        //         dataRFC({ rfc: token })
        //     }
        } catch (error) {
            console.warn(error);
        }
    }

    const _onPress = async () => {
        logOut({ rfc: token })
    }

    return (
        <Root>
            <ScrollView style={{ width: '100%' }}>
            <ProfileHeadComponent />
                <FormContainer>
                    <Formik
                        enableReinitialize={true}
                        initialValues={{ 
                            name: state.data.nombre,
                            secondname: state.data.snombre,
                            lastname: state.data.paterno,
                            secondlastname: state.data.materno,
                            cellphone: state.data.telefono,
                            rfc: state.data.rfc,
                        }}
                        onSubmit={(values, actions) => {
                            // signUp({
                            //     name: values.name,
                            //     secondname: values.secondname,
                            //     lastname: values.lastname,
                            //     secondlastname: values.secondlastname
                            //     cellphone: values.cellphone
                            // });
                            setTimeout(() => {
                                actions.setSubmitting(false)
                            }, 2000);
                        }}
                        validationSchema={validationSchema}
                        >
                        {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors, touched, isValid, setFieldValue }) => (
                            <>
                                <ColumnContainer>
                                    <TitleInputContainer>
                                        <ETASimpleText size={12} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'left'}>Nombre</ETASimpleText>
                                    </TitleInputContainer>
                                    <ETATextInputFilled
                                        value={values.name}
                                        placeholder='Nombre'
                                        placeholderTextColor='#777'
                                        keyboardType='default'
                                        autoCapitalize='none'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={!true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={100}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={false} //password
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('name')}
                                        onBlur={handleBlur('name')}
                                        // selection='1, 4'// pendiente de conocer
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        errors.name
                                        ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.name}</ETASimpleText>
                                        : null
                                    }

                                    <TitleInputContainer>
                                        <ETASimpleText size={12} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'left'}>Segundo nombre</ETASimpleText>
                                    </TitleInputContainer>
                                    <ETATextInputFilled
                                        value={values.secondname}
                                        placeholder='Segundo nombre'
                                        placeholderTextColor='#777'
                                        keyboardType='default'
                                        autoCapitalize='none'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={!true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={100}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={false} //password
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('secondname')}
                                        onBlur={handleBlur('secondname')}
                                        // selection='1, 4'// pendiente de conocer
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        errors.secondname
                                        ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.secondname}</ETASimpleText>
                                        : null
                                    }
                                </ColumnContainer>
                                <ColumnContainer>
                                    <TitleInputContainer>
                                        <ETASimpleText size={12} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'left'}>Apellido paterno</ETASimpleText>
                                    </TitleInputContainer>
                                    <ETATextInputFilled
                                        value={values.lastname}
                                        placeholder='Apellido paterno'
                                        placeholderTextColor='#777'
                                        keyboardType='default'
                                        autoCapitalize='none'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={!true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={100}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={false} //password
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('lastname')}
                                        onBlur={handleBlur('lastname')}
                                        // selection='1, 4'// pendiente de conocer
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        errors.lastname
                                        ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.lastname}</ETASimpleText>
                                        : null
                                    }
                                                                    <TitleInputContainer>
                                                                        <ETASimpleText size={12} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'left'}>Apellido materno</ETASimpleText>
                                                                    </TitleInputContainer>
                                    <ETATextInputFilled
                                        value={values.secondlastname}
                                        placeholder='Apellido materno'
                                        placeholderTextColor='#777'
                                        keyboardType='default'
                                        autoCapitalize='none'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={!true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={100}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={false} //password
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('secondlastname')}
                                        onBlur={handleBlur('secondlastname')}
                                        // selection='1, 4'// pendiente de conocer
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        errors.secondlastname
                                        ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.secondlastname}</ETASimpleText>
                                        : null
                                    }
                                </ColumnContainer>
                                <ColumnContainer>
                                    <TitleInputContainer>
                                        <ETASimpleText size={12} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'left'}>Teléfono celular</ETASimpleText>
                                    </TitleInputContainer>
                                    <ETATextInputFilled
                                        value={values.cellphone}
                                        placeholder='Teléfono celular'
                                        placeholderTextColor='#777'
                                        keyboardType='phone-pad'
                                        autoCapitalize='none'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={!true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={10}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={false} //cellphone
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('cellphone')}
                                        onBlur={handleBlur('cellphone')}
                                        // selection='1, 4'// pendiente de conocer
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        errors.cellphone
                                        ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.cellphone}</ETASimpleText>
                                        : null
                                    }
                                </ColumnContainer>
                                <ColumnContainer>
                                    <TitleInputContainer>
                                        <ETASimpleText size={12} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'left'}>RFC</ETASimpleText>
                                    </TitleInputContainer>
                                    <ETATextInputFilled
                                        value={values.rfc}
                                        placeholder='RFC'
                                        placeholderTextColor='#777'
                                        keyboardType='phone-pad'
                                        autoCapitalize='none'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={!true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={10}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={false} //rfc
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('rfc')}
                                        onBlur={handleBlur('rfc')}
                                        // selection='1, 4'// pendiente de conocer
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        errors.rfc
                                        ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.rfc}</ETASimpleText>
                                        : null
                                    }
                                </ColumnContainer>
                                {/* <ETAButtonFilled title='ENVIAR' onPress={handleSubmit} disabled={isSubmitting} colorButton={themeContext.PRIMARY_COLOR} padding={isSubmitting ? 10 : 120} /> */}
                                <ETAButtonFilled title='Cerrar sesión' onPress={() => _onPress()} disabled={false} colorButton={themeContext.PRIMARY_COLOR} padding={99} />
                            </>
                        )}
                    </Formik>
                </FormContainer>
            </ScrollView>
        </Root>
      );
}

export default ProfileForm;