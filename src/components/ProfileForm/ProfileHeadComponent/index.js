import React, {  useState, useEffect, useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/native';
import { Platform, useColorScheme } from 'react-native';

const Root = styled.View`
    flex: 0.2;
    justifyContent: center;
    alignItems: center;
    width: 100%;
    padding-vertical: 20px;
    background-color: white;
`;
const HeaderLogo = styled.Image`
    border-radius: 20px;
`;

const ProfileHeadComponent = () => {
    const colorSchema = useColorScheme();

    return (
        <Root>
            <HeaderLogo 
                style={{
                    height: Platform.OS === 'ios' ? 60 : 60,
                    width: Platform.OS === 'ios' ? 60 : 60 }}
                source={colorSchema == 'dark' ? require('@assets/avatar.png') : require('@assets/avatar.png')}
                resizeMode={'cover'}
            />
            <HeaderLogo 
                style={{
                    height: Platform.OS === 'ios' ? 18 : 18,
                    width: Platform.OS === 'ios' ? 110 : 110,
                    marginTop: 15 }}
                source={colorSchema == 'dark' ? require('@assets/icons/app-icon.png') : require('@assets/icons/app-icon.png')}
                resizeMode={'cover'}
            />
        </Root>
    )
}

export default React.memo(ProfileHeadComponent)