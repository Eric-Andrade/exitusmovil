import React, { useContext } from 'react';
import { Dimensions } from 'react-native';
import styled, { ThemeContext } from 'styled-components/native';
import { ETASimpleText } from '@etaui';

const Root = styled.View`
    flex: 1;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
    alignSelf: center;
    alignContent: center;
    marginVertical: 10px;
`;
const BuildImage = styled.Image`
    width: 70px;
    height: 70px;
    margin: 20px;
`;

const NewCredit = () => {
    return (
        <Root>
            <BuildImage source={require('@assets/build.png')} />
            <ETASimpleText
                size={15}
                weight={Platform.OS === 'ios' ? '500' : '500'}
                color='gray'
                align={'center'}>
                Under construction
            </ETASimpleText>   
        </Root>
    );
}

export default NewCredit;