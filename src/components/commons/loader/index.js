import React, { useContext } from 'react';
import { ActivityIndicator } from 'react-native';
import { ThemeContext } from 'styled-components/native';

const Loader = () => {
    const themeContext = useContext(ThemeContext);

    return (
        <>
            <ActivityIndicator size='large' color={themeContext.EXITUS_COLOR} />
        </>
    );
}

export default Loader;