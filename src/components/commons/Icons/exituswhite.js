import * as React from "react"
import Svg, { G } from "react-native-svg"

function SvgComponent({ size, color, props }) {
  return <Svg width={200.667} height={40} viewBox="0 0 137 30" {...props}>
            <G transform="translate(0.000000,30.000000) scale(0.100000,-0.100000)" fill="#FFFFFF" stroke="none"></G>
        </Svg>
}

const ExitusWhiteSVGIcon = React.memo(SvgComponent)
export default ExitusWhiteSVGIcon
