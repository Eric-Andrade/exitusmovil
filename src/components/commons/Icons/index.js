import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Octicons from 'react-native-vector-icons/Octicons';
import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FovisssteSvgIcon from './fovissste'
import IsssteSvgIcon from './issste'
import ImssSvgIcon from './imss'
import PemexSVGIcon from './pemex'
import ExitusSVGIcon from './exitus'
import ExitusWhiteSVGIcon from './exituswhite'

export {
    Ionicons,
    FontAwesome,
    AntDesign,
    Octicons,
    Entypo,
    EvilIcons,
    Foundation,
    MaterialCommunityIcons,
    FovisssteSvgIcon,
    IsssteSvgIcon,
    ImssSvgIcon,
    PemexSVGIcon,
    ExitusSVGIcon,
    ExitusWhiteSVGIcon
}