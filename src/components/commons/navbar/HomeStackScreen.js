import React, { useContext } from 'react';
import { Easing } from 'react-native';
import { createStackNavigator, TransitionPresets, CardStyleInterpolators } from '@react-navigation/stack'; 
import styled, { ThemeContext } from 'styled-components';
import { FontAwesome, AntDesign, Entypo } from '@commons/Icons';
import HomeScreen from '@screens/HomeScreen';
import ProfileScreen from '@screens/ProfileScreen';
import NewCreditScreen from '@screens/NewCreditScreen';
import RenovationCreditScreen from '@screens/RenovationCreditScreen';

const HeaderLeftCard = styled.TouchableOpacity`
  marginTop: 25px;
  marginLeft: 10px;
  alignItems: center;
  height: 30px;
  width: 30px;
  borderRadius: 15px;
`;
const HeaderRight = styled.TouchableOpacity`
  marginRight: 15px;
`;
const HeaderLeft = styled.TouchableOpacity`
  marginLeft: 15px;
`;

const config = {
    animation: 'spring',
    config: {
      duration: 300,
      stiffness: 1000,
      damping: 150,
      mass: 3,
      overshootClamping: !true,
      restDisplacementThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };
  
const configClose = {
  animation: 'timing',
  config: {
    duration: 300,
    easing: Easing.linear
  }
};

const HomeStack = createStackNavigator();
const HomeStackScreen = () => {
  const themeContext = useContext(ThemeContext);

  return (
    <HomeStack.Navigator
      mode='modal'
      screenOptions={{
        gestureEnabled: true,
        gestureDirection: 'horizontal',
        headerTitleAlign: 'center',
        headerShown: true,
        transitionSpec: {
          open: config,
          close: configClose
        },
        headerStyle: {
          backgroundColor: themeContext.PRIMARY_COLOR,
          shadowColor: 'transparent'
        },
      }}
    >
      <HomeStack.Screen
        name='HomeScreen'
        component={HomeScreen}
        options={({ navigation, route }) => ({
          headerTransparent: !true,
          headerRight: () => (
            <HeaderRight onPress={() => navigation.navigate('ProfileScreen')}>
              <FontAwesome name='user-circle-o' size={25} color={themeContext.THIRD_BACKGROUND_COLOR_LIGHT}/>
            </HeaderRight>
          ),
          headerTintColor: 'white',
          // cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          
        })} 
      />

      <HomeStack.Screen
        name='ProfileScreen'
        component={ProfileScreen}
        options={({ navigation, route }) => ({
          headerTitle: 'Perfil',
          headerTransparent: !true,
          headerLeft: () => (
            <HeaderLeft onPress={() => navigation.goBack()}>
              <AntDesign name='left' size={25} color={themeContext.THIRD_BACKGROUND_COLOR_LIGHT}/>
            </HeaderLeft>
          ),
          headerTintColor: 'white',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
        })} 
      />

      <HomeStack.Screen
        name='NewCreditScreen'
        component={NewCreditScreen}
        options={({ navigation, route }) => ({
          headerTitle: 'Nuevo crédito',
          headerTransparent: !true,
          headerLeft: () => (
            <HeaderLeft onPress={() => navigation.goBack()}>
              <AntDesign name='left' size={25} color={themeContext.THIRD_BACKGROUND_COLOR_LIGHT}/>
            </HeaderLeft>
          ),
          headerTintColor: 'white',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
        })} 
      />

      <HomeStack.Screen
        name='RenovationCreditScreen'
        component={RenovationCreditScreen}
        options={({ navigation, route }) => ({
          headerTitle: '',
          headerShown: true,
          headerTransparent: true,
          headerRight: '',
          headerLeft: () => (
            <HeaderLeftCard onPress={() => navigation.goBack()}>
              <AntDesign name='closecircle' size={25} color={themeContext.THIRD_BACKGROUND_COLOR_LIGHT}/>
            </HeaderLeftCard>
          ),
          headerTintColor: themeContext.PRIMARY_TEXT_COLOR_LIGHT,
          // transitionSpec: {
          //     open: config,
          //     close: configClose
          // },
          cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
        })} 
      />
    </HomeStack.Navigator>
  );
}

export default HomeStackScreen;