import React, { useContext } from 'react';
import { Easing } from 'react-native';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import styled, { ThemeContext } from 'styled-components';
import { AntDesign } from '@commons/Icons';
import LoginScreen from '@screens/LoginScreen';
import SignUpScreen from '@screens/SignUpScreen';
import RecoveryPassScreen from '@screens/RecoveryPassScreen';
import UpdatePasswordScreen from '@screens/UpdatePasswordScreen';
import VerificationCodeScreen from '@screens/VerificationCodeScreen';
import NoticeOfPrivacyScreen from '@screens/NoticeOfPrivacyScreen';
import CodeConfirmationScreen from '@screens/CodeConfirmationScreen';
import PreValidationScreen from '@screens/PreValidationScreen';

const HeaderRight = styled.TouchableOpacity`
  marginRight: 15px
`;
const HeaderLeft = styled.TouchableOpacity`
  marginLeft: 15px
`;

const config = {
    animation: 'spring',
    config: {
      duration: 300,
      stiffness: 1000,
      damping: 150,
      mass: 3,
      overshootClamping: !true,
      restDisplacementThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };
  
const configClose = {
  animation: 'timing',
  config: {
    duration: 300,
    easing: Easing.linear
  }
};

const AuthStack = createStackNavigator();
const AuthStackScreen= () => {
  const themeContext = useContext(ThemeContext);

  return (
    <AuthStack.Navigator
      screenOptions={{
        headerTitleAlign: 'center',
        transitionSpec: {
          open: config,
          close: configClose
        },
      }}>
        <AuthStack.Screen 
          name='LoginScreen' 
          component={LoginScreen}
          options={{
            headerTitle: 'Iniciar sesión',
            headerShown: false,
            headerTransparent: true,
            headerStyle: {
                backgroundColor: 'transparent'
            },
            headerTintColor: 'red',
            cardStyleInterpolator: CardStyleInterpolators.forFadeFromBottomAndroid
          }} 
        />

        <AuthStack.Screen 
          name='RecoveryPassScreen'
          component={RecoveryPassScreen}
          options={({ navigation, route }) => ({
            headerTitle: 'Restablecer contraseña',
            headerTransparent: !true,
            headerLeft: () => (
              <HeaderLeft onPress={() => navigation.goBack()}>
                <AntDesign name='left' size={25} color={themeContext.THIRD_BACKGROUND_COLOR_LIGHT}/>
              </HeaderLeft>
            ),
            headerStyle: {
              backgroundColor: themeContext.PRIMARY_COLOR
            },
            headerTintColor: 'white',
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          })}
        />
        
        <AuthStack.Screen 
          name='VerificationCodeScreen'
          component={VerificationCodeScreen}
          options={({ navigation, route }) => ({
            headerTitle: 'Confirmación de código',
            headerTransparent: !true,
            headerLeft: () => (
              <HeaderLeft onPress={() => navigation.goBack()}>
                <AntDesign name='left' size={25} color={themeContext.THIRD_BACKGROUND_COLOR_LIGHT}/>
              </HeaderLeft>
            ),
            headerStyle: {
              backgroundColor: themeContext.PRIMARY_COLOR
            },
            headerTintColor: 'white',
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          })}
        />
        
        <AuthStack.Screen 
          name='UpdatePasswordScreen'
          component={UpdatePasswordScreen}
          options={({ navigation, route }) => ({
            headerTitle: 'Restablecer contraseña',
            headerTransparent: !true,
            headerLeft: () => (
              <HeaderLeft onPress={() => navigation.goBack()}>
                <AntDesign name='left' size={25} color={themeContext.THIRD_BACKGROUND_COLOR_LIGHT}/>
              </HeaderLeft>
            ),
            headerStyle: {
              backgroundColor: themeContext.PRIMARY_COLOR
            },
            headerTintColor: 'white',
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          })}
        />

        <AuthStack.Screen 
          name='SignUpScreen' 
          component={SignUpScreen}
          options={({ navigation, route }) => ({
            headerTitle: 'Nueva cuenta',
            headerTransparent: !true,
            headerLeft: () => (
              <HeaderLeft onPress={() => navigation.goBack()}>
                <AntDesign name='left' size={25} color={themeContext.THIRD_BACKGROUND_COLOR_LIGHT}/>
              </HeaderLeft>
            ),
            headerStyle: {
                backgroundColor: themeContext.PRIMARY_COLOR
            },
            headerTintColor: 'white',
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          })} 
        />

        <AuthStack.Screen 
          name='NoticeOfPrivacyScreen' 
          component={NoticeOfPrivacyScreen}    
          options={({ navigation, route }) => ({
            headerTransparent: !true,
            headerTitle: 'Términos y Condiciones',
            headerLeft: () => (
              <HeaderLeft onPress={() => navigation.goBack()}>
                <AntDesign name='left' size={25} color={themeContext.THIRD_BACKGROUND_COLOR_LIGHT}/>
              </HeaderLeft>
            ),
            headerStyle: {
                backgroundColor: themeContext.PRIMARY_COLOR
            },
            headerTintColor: 'white',
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          })}
        />

        <AuthStack.Screen 
          name='CodeConfirmationScreen' 
          component={CodeConfirmationScreen}        
          options={({ navigation, route }) => ({
            headerTransparent: !true,
            headerTitle: 'Código de verificación',
            headerLeft: () => (
              <HeaderLeft onPress={() => navigation.goBack()}>
                <AntDesign name='left' size={25} color={themeContext.THIRD_BACKGROUND_COLOR_LIGHT}/>
              </HeaderLeft>
            ),
            headerStyle: {
              backgroundColor: themeContext.PRIMARY_COLOR
            },
            headerTintColor: 'white',
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          })} 
        />

        <AuthStack.Screen 
          name='PreValidationScreen' 
          component={PreValidationScreen}        
          options={({ navigation, route }) => ({
            headerTransparent: !true,
            headerTitle: 'Código de verificación',
            headerShown: false,
            headerStyle: {
              backgroundColor: themeContext.PRIMARY_COLOR
            },
            headerTintColor: 'white',
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
          })} 
        />
    </AuthStack.Navigator>
  );
}

export default AuthStackScreen;