import React, { useEffect, useContext } from 'react';
import { StatusBar, useColorScheme } from 'react-native';
import { ETANetInfo } from '@etaui';
import { Context } from '@context';
import HomeStackScreen from './HomeStackScreen';
import AuthTabNavigator from './AuthTabNavigator';
import SplashScreen from '@components/commons/SplashScreen';
import { lightTheme, darkTheme } from '@utils/constants';

const NavBar = () => {
  const { restoreToken, state } = useContext(Context);
  const colorSchema = useColorScheme();
  
  useEffect(() => {
    let isUnmount = false
    setTimeout(() => {
      restoreToken();
    }, 2000);
    return () => {
      isUnmount = true
    }
  }, [])
  
  if (state.isLoading) {
    return <SplashScreen />
  }

  return (
    <>
      <StatusBar backgroundColor={colorSchema == 'dark' ? lightTheme.PRIMARY_COLOR : lightTheme.PRIMARY_COLOR } barStyle='light-content' hidden={false} />
      <ETANetInfo />
      {
        state.userToken !== null
        ? <HomeStackScreen />
        : <AuthTabNavigator />
      }
    </>
  );
}

export default NavBar;