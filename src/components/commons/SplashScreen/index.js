import React from 'react';
import { StatusBar } from 'react-native';
import styled from 'styled-components/native';

const Root = styled.View`
    flex: 1;
    flexDirection: column;
    display: flex;
    justifyContent: center;
    alignItems: center;
    backgroundColor: ${props => props.theme.EXITUS_COLOR};
`; 
const Logo = styled.Image`
    height: ${Platform.OS === 'ios' ? 35 : 35}px;
    width: ${Platform.OS === 'ios' ? 170 : 170}px;
`;

const SplashScreen = () => {
    return (
        <Root>
            <StatusBar hidden={true} />
            <Logo source={require('@assets/343x75.png')}/>
        </Root>
    );
}

export default SplashScreen;