import React, { useState, useEffect, useContext } from 'react';
import { ScrollView, Platform, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { Formik } from 'formik';
import {useNavigation} from '@react-navigation/native';
import * as yup from 'yup';
import styled, { ThemeContext } from 'styled-components/native';
import { ETASimpleText, ETATextInputFilled, ETAButtonFilled, ETAModal } from '@etaui';
import {useModal} from '@etaui/modal/useModal';
import { Context } from '@context';

const Root = styled.View`
    flex: 1.15;
    flexDirection: column;
    justifyContent: flex-end;
    alignItems: flex-end;
    marginTop: 30px
`;
const MessageContainer = styled.View`
    flex: 1;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
    margin: 20px;
    paddingHorizontal: 30px;
    background-color: transparent;
`;
const HeadTextContainer = styled.View`
    margin-top: 10px;
    padding-vertical: 2px;
    background-color: transparent;
`;
const FormContainer = styled.View`
    flex: 1;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
    alignSelf: center;
    alignContent: center;
`;

const validationSchema = yup.object().shape({
    cellphone: yup
        .string()
        .matches(/^[0-9]*$/, 'Ingrese únicamente números')
        .min(10, 'Teléfono celular debe tener 10 dígitos')
        .max(10, 'Teléfono celular debe tener 10 dígitos')
        .typeError('Ingrese únicamente números')
        .required('Este campo no puede estar vacío'),
    email: yup
        .string()
        .email('Ingrese un email válido'),
});

const RecoveryPassForm = () => {
    const themeContext = useContext(ThemeContext);
    const navigation = useNavigation();
    const { updatePassword, state } = useContext(Context);
    const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
    const [loading, setloading] = useState(false);

    useEffect(() => {
        let isUnmount = false
        console.log('RecoveryPassForm state: ', state.isLoading);
        // toggleModal(state.error);
        setloading(state.isLoading)
        return () => {
          isUnmount = true
        }
    }, [state])

    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()} style={{ flex: 1 }}>
            <Root>
                {
                    state.error 
                    ?   <ETAModal
                            // text='Por favor corrobora tus datos e intentalo de nuevo.'
                            text={state.message.charAt(0).toUpperCase() + state.message.toLowerCase().slice(1)}
                            isActive={itemModalOpen}
                            handleClose={() => setItemModalOpen(false)}
                            textColor={themeContext.PRIMARY_TEXT_COLOR_LIGHT}
                            textButton='De acuerdo'
                        />
                    :   null
                }
                <ScrollView>
                    <MessageContainer>
                        <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='center' >
                            Por favor ingrese el teléfono de celular y el email registrados con su cuenta.
                        </ETASimpleText>
                        
                    <HeadTextContainer>
                        <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.SECONDARY_BACKGROUND_COLOR} align='left' >Campos obligatorios *</ETASimpleText>
                    </HeadTextContainer>
                    </MessageContainer>
                    <FormContainer>
                        <Formik
                            enableReinitialize={true}
                            initialValues={{ 
                                cellphone: '', // 1234567890
                                email: '',
                            }}
                            onSubmit={(values, actions) => { 
                                updatePassword({ cellphone: values.cellphone, email: values.email, pass: 1, op: 2, screen: 'CodeConfirmationScreen' })
                                toggleModal(true);
                                // actions.setSubmitting(loading ? loading : true)
                                
                                actions.setSubmitting(false)
                                
                            }}
                            validationSchema={validationSchema}
                            >
                            {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors }) => (
                                <>
                                    <ETATextInputFilled
                                        value={values.cellphone}
                                        placeholder='Teléfono celular *'
                                        placeholderTextColor='#777'
                                        keyboardType='phone-pad'
                                        autoCapitalize='none'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={10}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={false} //password
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('cellphone')}
                                        // onBlur={handleBlur('cellphone')}
                                        // selection='1, 4'//? no sé we xd
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        errors.cellphone
                                        ?   <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.cellphone}</ETASimpleText>
                                        :   null
                                    }
                                    <ETATextInputFilled
                                        value={values.email}
                                        placeholder='Email'
                                        placeholderTextColor='#777'
                                        keyboardType='email-address'
                                        autoCapitalize='characters'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={100}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={false} //password
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('email')}
                                        // onBlur={handleBlur('email')}
                                        // selection='1, 4'//? no sé we xd
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        errors.email
                                        ?   <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.email}</ETASimpleText>
                                        :   null
                                    }
                                    <ETAButtonFilled title='Continuar' onPress={handleSubmit} disabled={isSubmitting ? true : false} colorButton={themeContext.PRIMARY_COLOR} padding={isSubmitting ? 10 : 105} />
                                </>
                            )}
                        </Formik>
                    </FormContainer>
                </ScrollView>
            </Root>
        </TouchableWithoutFeedback>
    );
}

export default RecoveryPassForm;