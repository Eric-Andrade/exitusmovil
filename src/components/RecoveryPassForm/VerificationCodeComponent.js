import React, { useContext } from 'react';
import { ScrollView, Platform, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { Formik } from 'formik';
import {useNavigation} from '@react-navigation/native'
import * as yup from 'yup';
import styled, { ThemeContext } from 'styled-components/native';
import { ETASimpleText, ETATextInputFilled, ETAButtonFilled, ETAErrorMessage } from '@etaui';
import { Context } from '@context';

const Root = styled.View`
    flex: 1.15;
    flexDirection: column;
    justifyContent: flex-end;
    alignItems: flex-end;
    marginTop: 30px
`;
const MessageContainer = styled.View`
    flex: 0.1;
    margin: 20px;
    paddingHorizontal: 20px;
`;
const FormContainer = styled.View`
    flex: 1;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
    alignSelf: center;
    alignContent: center;
`;

const validationSchema = yup.object().shape({
    code: yup
        .string()
        .required('Este campo no puede estar vacío')
});

const VerificationCodeComponent = () => {
    const themeContext = useContext(ThemeContext);
    const navigation = useNavigation()
    const { updatePassword, state } = useContext(Context);

    // useEffect(() => {
    //     console.log('VerificationCodeComponent state: ', state);
    //     updatePassword({ cellphone: '', pass: 1, op: 2 })
    //     if (!state.data.error) {
    //         navigation.navigate('UpdatePasswordScreen')
    //     }
    // }, [])

    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()} style={{ flex: 1 }}>
            <Root>
                <ScrollView>
                    <MessageContainer>
                        <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' >
                            Hemos enviado un código de verificación por medio de SMS al télefono proporcionado y al correo indicado, favor de ingresarlo a continuación.
                        </ETASimpleText>
                    </MessageContainer>
                    <FormContainer>
                        <Formik
                            enableReinitialize={true}
                            initialValues={{ 
                                code: ''
                            }}
                            onSubmit={(values, actions) => {
                                updatePassword({ cellphone: 1, email: '', pass: values.code, op: 1, screen: 'UpdatePasswordScreen' })
                                actions.setSubmitting(false)
                            }}
                            validationSchema={validationSchema}
                            >
                            {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors, touched }) => (
                                <>
                                    <ETATextInputFilled
                                        value={values.code}
                                        placeholder='Code *'
                                        placeholderTextColor='#777'
                                        keyboardType='default'
                                        autoCapitalize='none'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={5}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={!true} //code
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('code')}
                                        onBlur={handleBlur('code')}
                                        // selection='1, 4'// pendiente de conocer
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        errors.code
                                        ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.code}</ETASimpleText>
                                        : null
                                    }
                                    
                                    <ETAButtonFilled title='CONTINUAR' onPress={handleSubmit} disabled={isSubmitting ? true : false} colorButton={themeContext.PRIMARY_COLOR} padding={isSubmitting ? 10 : 105} />
                                </>
                            )}
                        </Formik>
                    </FormContainer>
                </ScrollView>
            </Root>
        </TouchableWithoutFeedback>
    );
}

export default VerificationCodeComponent;