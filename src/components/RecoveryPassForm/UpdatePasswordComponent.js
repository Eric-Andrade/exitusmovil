import React, { useEffect, useContext } from 'react';
import { ScrollView, Platform, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { Formik } from 'formik';
import {useNavigation, useRoute} from '@react-navigation/native'
import * as yup from 'yup';
import styled, { ThemeContext } from 'styled-components/native';
import { ETASimpleText, ETATextInputFilled, ETAButtonFilled, ETAErrorMessage } from '@etaui';
import { Context } from '@context';

const Root = styled.View`
    flex: 1.15;
    flexDirection: column;
    justifyContent: flex-end;
    alignItems: flex-end;
    marginTop: 30px
`;
const MessageContainer = styled.View`
    flex: 0.1;
    margin: 20px;
    paddingHorizontal: 20px;
`;
const FormContainer = styled.View`
    flex: 1;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
    alignSelf: center;
    alignContent: center;
`;

const validationSchema = yup.object().shape({
    password: yup
        .string()
        .required('Este campo no puede estar vacío'),
    confirmpassword: yup
        .string()
        .oneOf([yup.ref('password')], 'Las contraseñas no coinciden')
        .required('Este campo no puede estar vacío'),
});

const UpdatePasswordComponent = () => {
    const themeContext = useContext(ThemeContext)
    const navigation = useNavigation()
	const route = useRoute()
    const { updatePassword, state } = useContext(Context)
    const { propcellphone } = route?.params

    useEffect(() => {
        let isUnmount = false
        console.log('+++UpdatePasswordComponent state: ', state);
        if (!state.data.error && state.data.respuesta === 'OK') {
            // navigation.navigate('UpdatePasswordScreen')
        }
        return () => {
          isUnmount = true
        }
    }, [state.data.error, state.data.respuesta])

    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()} style={{ flex: 1 }}>
            <Root>
                <ScrollView>
                    <MessageContainer>
                        <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='center' >
                            A continuación ingrese por favor su nueva contraseña. Después escribala nuevamente para validar que sea correcta.
                        </ETASimpleText>
                    </MessageContainer>
                    <FormContainer>
                        <Formik
                            enableReinitialize={true}
                            initialValues={{ 
                                password: ''
                            }}
                            onSubmit={(values, actions) => {
                                updatePassword({ cellphone: propcellphone, email: '', pass: values.password, op: 3, screen: 'LoginScreen' })
                                actions.setSubmitting(false)
                            }}
                            validationSchema={validationSchema}
                            >
                            {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors, touched }) => (
                                <>
                                    <ETATextInputFilled
                                        value={values.password}
                                        placeholder='Contraseña *'
                                        placeholderTextColor='#777'
                                        keyboardType='default'
                                        autoCapitalize='characters'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={100}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={!true} //password
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('password')}
                                        onBlur={handleBlur('password')}
                                        // selection='1, 4'// pendiente de conocer
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        errors.password
                                        ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.password}</ETASimpleText>
                                        : null
                                    }
                                    <ETATextInputFilled
                                        value={values.confirmpassword}
                                        placeholder='Repetir contraseña *'
                                        placeholderTextColor='#777'
                                        keyboardType='default'
                                        autoCapitalize='none'
                                        autoCapitalize='characters'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={100}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={!true} //password
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('confirmpassword')}
                                        onBlur={handleBlur('confirmpassword')}
                                        // selection='1, 4'// pendiente de conocer
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        touched.confirmpassword && errors.confirmpassword
                                        ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.confirmpassword}</ETASimpleText>
                                        : null
                                    }
                                    <ETAButtonFilled title='Actualizar contraseña' onPress={handleSubmit} disabled={isSubmitting ? true : false} colorButton={themeContext.PRIMARY_COLOR} padding={isSubmitting ? 10 : 105} />
                                </>
                            )}
                        </Formik>
                    </FormContainer>
                </ScrollView>
            </Root>
        </TouchableWithoutFeedback>
    );
}

export default UpdatePasswordComponent;