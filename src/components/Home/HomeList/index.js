import React, { useState, useEffect, useContext } from 'react';
import { FlatList, RefreshControl, Dimensions, Platform } from 'react-native';
import styled, { ThemeContext } from 'styled-components/native';
import { useNavigation, useIsFocused } from '@react-navigation/native';
import axios from 'axios';
import { ETACard, ETAHeaderText, ETASimpleText, ETALink, ETAModal } from '@etaui'
import {useModal} from '@etaui/modal/useModal';
import Loader from '@commons/loader';
import AsyncStorage from '@react-native-community/async-storage';
import { vAndroid, vIOS, URL2, URL3 } from '@utils/constants';

const { width } = Dimensions.get('window')

const Root = styled.View`
    flex: 1;
    justifyContent: center;
    alignItems: center;
    zIndex: 10;
`;
const HeaderContainer = styled.View`
    justifyContent: center;
    alignSelf: flex-start;
    padding: 10px 15px;
    marginTop: 30px;
`;
const EmptyContainer = styled.View`
    flex: 1;
    flexDirection: column;
    alignItems: center;
    alignSelf: center;
    justifyContent: flex-start;
    paddingVertical: 15px;
    width: ${width - 100}px;
`;
const EmptyMessageContainer = styled.View`
    flexDirection: column;
    alignItems: center;
    justifyContent: center;
    paddingVertical: 10px;
    marginTop: 30px;
    width: 100%;
`;
const WalletImageContainer = styled.View`
    justifyContent: center;
    alignItems: center;
    padding: 10px;
    border-radius: 40px;
    background-color: white;
`;
const WalletImage = styled.Image`
    width: 60px;
    height: 60px;
`;
const Touchable = styled.TouchableOpacity``;

const HomeList = () => {
    const themeContext = useContext(ThemeContext);
    const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
    const navigation = useNavigation();
    const isFocused = useIsFocused();
    const [ fetching, setfetching ] = useState(!true);
    const [ items, setitems ] = useState(null);
    const [ version, setversion ] = useState(null);


    useEffect(() => {
        let isUnmount = false
        if (isFocused) {
            _getCredits()
            _verificaVersion({ version: Platform.OS === 'ios' ? vIOS : vAndroid, plataforma: Platform.OS === 'ios' ? 1 : 3 })
            if (!version) {
                toggleModal(true);
            }
        }
        return () => {
          isUnmount = true
        }
    }, [isFocused, version]);

    const _getCredits = async () => {
        setfetching(true);
        
        try {
            const PersonId = await AsyncStorage.getItem('@PersonId')
            console.log('[_getCredits] PersonId', PersonId);

            if (PersonId) {
                const credits = await axios.get(`${URL2}api/Auth/Renovation?PersonId=${PersonId}`)
                const rejectedCredits = await axios.get(`${URL2}api/Auth/RenovationRejected?PersonId=${PersonId}`)
                
                // let creditsJSON = [{"Frecuencia": "Mensual", "iCreditId": "459421", "nDeposit": "31221", "iErrorCode": "0", "iOpRejected": "306", "iOpStatus": "1", "iPayments": "24", "nAmount": "29001", "nPayment": "3200.73", "vErrorDescription": "", "vRejected": "Saldo vencido"}, {"Frecuencia": "Mensual", "iCreditId": "539983","nDeposit": "31221", "iErrorCode": "0", "iOpRejected": "306", "iOpStatus": "1", "iPayments": "24", "nAmount": "20000", "nPayment": "2167.33", "vErrorDescription": "", "vRejected": "Saldo vencido"}] // créditos por aprobar setteados
                let creditsJSON = JSON.parse(credits.data.toString())
                let rejectedCreditsJSON = JSON.parse(rejectedCredits.data.toString())
                    if (creditsJSON[0].vErrorDescription === '' && rejectedCreditsJSON[0].vErrorDescription === '') {
                        setfetching(!true);
                        console.log('cayó en if y sí hay de ambos casos');
                        let newArray = [ ...creditsJSON, ...rejectedCreditsJSON ]
                        console.log('_getCredits newArray: ', newArray);
                        setitems(newArray);
                        
                    } else if(creditsJSON[0].vErrorDescription === '' && rejectedCreditsJSON[0].vErrorDescription !== ''){
                        console.log('credits rejected viene vacio');
                        setfetching(!true);
                        setitems(creditsJSON)
                    } else if(rejectedCreditsJSON[0].vErrorDescription === '' && creditsJSON[0].vErrorDescription !== '') {
                        console.log('credits viene vacio', rejectedCreditsJSON);
                        setfetching(!true);
                        setitems(rejectedCreditsJSON)
                    } else {
                        console.log('Ambos vienen vacios');
                        setfetching(!true);
                        setitems([]);                        
                    }
            } else {
                console.log('cayó en else');
                setfetching(!true);
                setitems([])
            }
        } catch (error) {
            console.warn('HomeScreen HomeLists _getCredits error: ', error);
        }
    }

    const _verificaVersion = async ({ version, plataforma }) => {
        try {
            let { data } = await axios.get(`${URL3}verificaVersion?version=${version}&plataforma=${plataforma}`);
            let v = JSON.parse(data)
            console.log('verificaVersion data: ', v.version);
            setversion(v.version)
        } catch (error) {
            console.log('verificaVersion error: ', error);
        }
    }

    const handleRefreshing = () => {
        setfetching(true);
        _getCredits()
        setTimeout(() => {
            setfetching(!true);
        }, 1000);
    }

    const _onPress = (item) => {
        navigation.navigate('RenovationCreditScreen', {
            screen: 'HomeScreen',
            params: {
              item: item
            }
        });
    }

    if (fetching) {
        return <Root><Loader /></Root>
    }

    return (
        <>
            {
                version === false
                ?   <ETAModal
                        text='Hay una nueva versión de la aplicación.'
                        isActive={itemModalOpen}
                        handleClose={() => setItemModalOpen(false)}
                        textColor={themeContext.PRIMARY_TEXT_COLOR_LIGHT}
                        textButton='De acuerdo'
                    />
                :   null
            }
            {
                items !== null
                ?   items.length !== 0
                    ?   <Root>
                            <HeaderContainer>
                                <ETAHeaderText size={16} weight='bold' color={themeContext.SECONDARY_BACKGROUND_COLOR} align={'left'}>
                                    {
                                        items.length === 1
                                        ?   `${items.length} crédito`
                                        :    `${items.length} créditos`
                                    }
                                </ETAHeaderText>
                            </HeaderContainer>
                            <FlatList 
                                contentContainerStyle={{
                                    alignSelf: 'stretch'
                                }}
                                showsVerticalScrollIndicator={false}
                                data={items}
                                keyExtractor={(item, index) => index.toString()}
                                initialNumToRender={4}
                                renderItem={({ item }) => {
                                    return (
                                        <Touchable
                                            onPress={() => _onPress(item)}>
                                            <ETACard {...item} />
                                        </Touchable>
                                    );
                                }}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={fetching}
                                        onRefresh={() => handleRefreshing()}
                                        // title='Pull to refresh'
                                        tintColor={themeContext.EXITUS_COLOR}
                                        titleColor={themeContext.EXITUS_COLOR}
                                        colors={[themeContext.EXITUS_COLOR]} 
                                    />
                                }
                            />
                        </Root>
                :   <EmptyContainer>
                        <EmptyMessageContainer>
                            <WalletImageContainer>
                                <WalletImage source={require('@assets/wallet2.png')} resizeMode={'cover'} />
                            </WalletImageContainer>
                            <ETASimpleText size={16} weight='400' color={themeContext.SECONDARY_BACKGROUND_COLOR} align={'center'}
                                style={{ marginVertical: 15 }}>
                                Al parecer aún no cuenta con créditos.
                            </ETASimpleText>
                            {/* <ETAButtonFilled title='SOLICITAR UN CRÈDITO' onPress={() => console.log('request credit pressed')} colorButton={themeContext.PRIMARY_COLOR} padding={70} /> */}
                        </EmptyMessageContainer>
                        <ETASimpleText size={13} weight='400' color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'center'}>
                            Si está presentando problemas, por favor comuniquese al teléfono <ETALink url={`tel:5544408080`} text={'55 4440 8080'} size={12} weight={Platform.OS === 'ios' ? '600' : '700'} color={themeContext.SECONDARY_BACKGROUND_COLOR} align='left' />
                        </ETASimpleText>
                    </EmptyContainer>
                :   <></>
            }
        </> 
    );
}

export default React.memo(HomeList);