import React, { useState, useEffect, useContext } from 'react';
import { Platform, Dimensions } from 'react-native';
import styled, { ThemeContext } from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';
import { ETASimpleText, ETACarousel } from '@etaui';

const { width } = Dimensions.get('window')

const Root = styled.View`
    alignItems: center;
    backgroundColor: transparent;
`;
const HeadImageContainer = styled.View`S
`;
const HeadImage = styled.Image`
    width: ${width}px;
    height: 170px;
`;
const HeadBottomContainer = styled.View`
    flex: 1;
    width: 100%;
    justifyContent: center;
    alignItems: center;
`;
const UserDataContainer = styled.View` 
    position: absolute;
    width: 220px;
    height: 40px;
    bottom: 15px;
    justifyContent: center;
    alignItems: center;
    zIndex: 1000;
`;
const UserContainer = styled.View`
    min-width: 220px;
    height: 35px;
    position: relative;
    padding-horizontal: 20px;
    justifyContent: center;
    alignItems: center;
    alignSelf: center;
    borderRadius: 20px;
    shadowColor: black;
    shadowOffset: 1px 4.5px;
    shadowRadius: 4px;
    shadowOpacity: 0.1;
    elevation: 5;
    zIndex: 1000;
    backgroundColor: white;
`;
const RequestCreditContainer = styled.TouchableOpacity`
    min-width: 220px;
    height: 35px;
    position: relative;
    padding-horizontal: 20px;
    justifyContent: center;
    alignItems: center;
    alignSelf: center;
    borderRadius: 20px;
    shadowColor: black;
    shadowOffset: 1px 4.5px;
    shadowRadius: 4px;
    shadowOpacity: 0.1;
    elevation: 5;
    zIndex: 1000;
    backgroundColor: white;
`;

const HomeHead = ({ userData }) => {
    const themeContext = useContext(ThemeContext);
    const navigation = useNavigation();
    const [images ] = useState([
        {
            _id: 1,
            image: 'https://www.homecareassistanceroseville.com/wp-content/uploads/2018/10/Senior-Enjoying-Dinner.jpg'
        },
        {
            _id: 2,
            image: 'https://www.bls.gov/careeroutlook/2017/images/older-workers_cover.jpg'
        },
        {
            _id: 3,
            image: 'https://cdn.aarp.net/content/dam/aarp/work/small-business/2019/02/1140-small-business-owners-older-workers.jpg'
        }
    ])

    return (
        <Root> 
            <HeadImageContainer>
                {/* <HeadImage source={require('@assets/img4.jpg')} /> */}
                <ETACarousel items={images} autoplay={!true} time={7000} />
            </HeadImageContainer>
            <HeadBottomContainer>
                {/* <UserDataContainer>
                    <ETASimpleText
                        size={16}
                        weight={Platform.OS === 'ios' ? '800' : 'bold'}
                        color='white'
                        align={'center'}
                        style={{
                            textShadowColor: 'rgba(0, 0, 0, 0.7)',
                            textShadowOffset: {width: -1, height: 1.5},
                            textShadowRadius: 1
                        }}>
                        {userData}
                    </ETASimpleText>   
                </UserDataContainer> */}
                <UserContainer 
                    // onPress={() => navigation.navigate('NewCreditScreen')}
                    onPress={() => navigation.navigate('ProfileScreen')}
                >
                    <ETASimpleText
                        size={14}
                        weight={Platform.OS === 'ios' ? '400' : '300'}
                        color='#595959'
                        align={'center'}>
                        {/* Solicitar un crédito */}
                        {userData.toUpperCase()}
                    </ETASimpleText>
                </UserContainer>
            </HeadBottomContainer>
        </Root>
    );
}

export default HomeHead;