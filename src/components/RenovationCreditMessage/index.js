import React, { useState, useContext, useEffect } from 'react';
import { Dimensions, Alert, Platform }  from 'react-native';
import styled, { ThemeContext } from 'styled-components/native';
import { useRoute } from '@react-navigation/native';
import { ETASimpleText, ETAHeaderText, ETAButtonFilled, ETALink, ETAModal } from '@etaui';
import {useModal} from '@etaui/modal/useModal';
import { Context } from '@context';
import { currencySeparator } from '@functions';
import axios from 'axios';
import { URL4 } from '@utils/constants';
import AsyncStorage from '@react-native-community/async-storage';

const { width } = Dimensions.get('window')

const Root = styled.View`
    flex: 1;
    flexDirection: column;
    width: 100%;
    backgroundColor: ${props => props.theme.PRIMARY_TEXT_BACKGROUND_COLOR};
`;
const HeaderTextContainer = styled.View`
    flex: 0.3;
    justifyContent: center;
    alignItems: center;
`;
const TagContainer = styled.View`
    flexDirection: row;
    justifyContent: center;
    alignItems: center;
    paddingHorizontal: 20px;
    height: 26px;
    borderRadius: 5px;
    borderWidth: 0.5px;
    marginTop: 10px;
`;
const MessageContainer = styled.View`
    flex: 1;
    marginTop: 40px;
    alignItems: center;
    alignSelf: center;
    width: ${width - 50}px;
    borderRadius: 5px;
    borderWidth: 0px;
    borderColor: #333;
    shadowColor: #000;
    shadowOpacity: 0;
    shadowRadius: 3px;
    elevation: 0;
`;
const RejectedMessageContainer = styled.View`
    width: 90%;
`;
const RejectedInstructionsContainer = styled.View`
    paddingHorizontal: 20px;
    width: 90%;
`;

const RenovationCreditMessage = () => {
    const themeContext = useContext(ThemeContext);
    const { renovatedCredits, operaRenovacion, state } = useContext(Context);
    const route = useRoute();
    const { item } = route.params.params;
    const [ isSubmitting, setisSubmitting ] = useState(false);
    const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();

    useEffect(() => {
        let isUnmount = false
        renovatedCredits({CreditId: item.iCreditId})
        console.log('se ejecutó renovatedCredits', { item });
        return () => {
          isUnmount = true
        }
    }, [])

    const handleSubmit = (iCreditId) => {        
        Alert.alert(
            `Confirmar renovación del  crédito ${item.iCreditId}.`,
            'Por favor presione Renovar para confirmar la renovación del crédito.',
            [
              {
                text: 'Cancelar',
                onPress: () => console.log('Cancelar Pressed'),
                style: 'cancel'
              },
              { text: 'Renovar', onPress: () => submitFunction(iCreditId) }
            ],
            { cancelable: false }
        );
    }

    const submitFunction = (iCreditId) => {
        LogRenovation()
        operaRenovacion({ CreditId: iCreditId })
    }

    const LogRenovation = async () => {
        const PersonId = await AsyncStorage.getItem('@PersonId')
        const userTelefono = await AsyncStorage.getItem('@userTelefono')
        let today = new Date()
        let nowDate = `${today.getFullYear()}-${('0' + (today.getMonth() + 1)).slice(-2)}-${('0' + (today.getDate())).slice(-2)}`
        let params = {
            fecha_invitacion: nowDate,
            id_cliente: PersonId,
            oferta: {
              id_credito_origen: item.iCreditId,
              monto: item.nAmount,
              pago: item.nDeposit,
              plazo: `${item.iPayments} meses`
            },
            telefono: userTelefono,
            ok_aceptacion: 'BotonAppRenovación',
            id_credito_nuevo: 0,
            fecha_hora_aceptacion: `${nowDate} ${('0' + (today.getHours())).slice(-2)}:${('0' + (today.getMinutes())).slice(-2)}`,
            texto: 'Acepto la renovación del crédito',
            identificador_wa: 'APPMOVILRENOVA'
        }

        console.log('[LogRenovation] params: ', params);

        try {
            const { data } = await axios.post(`${URL4}`, params);
            console.log('[LogRenovation] data', data);
        } catch (error) {
            console.log('[LogRenovation] error', error);
        }
    }

    return (
        <>
            <Root>
                <HeaderTextContainer
                    style={{ backgroundColor: item.nDeposit ? themeContext.SUCCESS_COLOR : themeContext.FAIL_COLOR }}>
                    <ETAHeaderText size={14} weight='600' color={themeContext.THIRD_BACKGROUND_COLOR_LIGHT} align={'center'}>
                        Crédito
                    </ETAHeaderText>
                    <ETAHeaderText size={30} weight='600' color={themeContext.THIRD_BACKGROUND_COLOR_LIGHT} align={'center'}>
                        {item.iCreditId}
                    </ETAHeaderText>

                    <TagContainer
                        style={{ borderColor: item.nDeposit ? '#b3e6bb' : '#ffcccc' }}
                    >
                        <ETASimpleText size={15} weight='200' color={ item.nDeposit ? themeContext.THIRD_BACKGROUND_COLOR_LIGHT : themeContext.THIRD_BACKGROUND_COLOR_LIGHT } align='center' >
                        {
                            item.nDeposit
                            ?   'Válido para renovación'
                            :   item.vRejected
                        }
                        </ETASimpleText>
                    </TagContainer>
                    
                </HeaderTextContainer>
                   
                <MessageContainer 
                    style={{
                        shadowOffset: { width: 3, height: 5 },
                        height: item.nDeposit ? '50%' : '65%'
                    }}
                > 
                    <ETASimpleText size={item.nDeposit ? 15 : 24} weight={item.nDeposit ? '300' : '600'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'center'}> 
                        {
                            item.nDeposit
                            ?   `El crédito ${item.iCreditId} se renovará por $${currencySeparator(parseFloat(item.nAmount).toFixed(2))} a un plazo de ${item.iPayments} meses, con pagos de $${currencySeparator(parseFloat(item.nPayment).toFixed(2))}.`
                            :   `Recuerde que...`
                        }
                    </ETASimpleText>
                    <ETASimpleText size={13} weight='bold' color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'left'}>
                        {' \n'}
                            {
                                item.nDeposit
                                ?   `La cuenta recibirá un depósito de $${currencySeparator(parseFloat(item.nDeposit).toFixed(2))}.`
                                :   'Para poder recibir una oferta de renovación Express:'
                            }
                        {' \n'}
                    </ETASimpleText>

                    {
                        item.nDeposit
                        ?    <>
                                <ETAButtonFilled title='Renovar ahora' onPress={() => handleSubmit(item.iCreditId)} disabled={isSubmitting ? true : false} colorButton={themeContext.SUCCESS_COLOR} padding={isSubmitting ? 10 : 75} />
                            </>
                        :   <RejectedMessageContainer>
                                <RejectedInstructionsContainer>
                                    <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '300' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'left'}>
                                    • Su  crédito debe tener tres o más meses de antigüedad.
                                    </ETASimpleText>
                                    <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '300' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'left'}>
                                    • Pagos siempre puntuales.
                                    </ETASimpleText>
                                    <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '300' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'left'}>
                                    • Un historial sano de crédito como cliente Exitus. {' \n'}
                                    </ETASimpleText>

                                </RejectedInstructionsContainer>
                                <ETASimpleText size={14} weight='400' color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align={'center'}>
                                    En caso de tener dudas o aclaraciones sobre sus créditos y pagos, comuniquese al teléfono <ETALink url={`tel:5544408080`} text={'55 4440 8080'} size={14} weight='700' color={themeContext.SECONDARY_BACKGROUND_COLOR} align='left' />, será un gusto atenderle.
                                </ETASimpleText>
                            </RejectedMessageContainer>
                    }
                </MessageContainer>
            </Root>
        </>
    );
}

export default RenovationCreditMessage;