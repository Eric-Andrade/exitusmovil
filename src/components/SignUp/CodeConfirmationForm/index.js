import React, { useState, useContext, useEffect } from 'react';
import styled, { ThemeContext } from 'styled-components/native';
import { Platform, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { Formik } from 'formik';
import { ETASimpleText, ETATextInputFilled, ETAButtonFilled, ETAModal, ETACircularProgress, ETAToast } from '@etaui';
import axios from 'axios';
import * as yup from 'yup';
import {useRoute} from '@react-navigation/native';
import {useModal} from '@etaui/modal/useModal';
import { Context } from '@context';
import { URL } from '@utils/constants';
import Animated, { Value, set, useCode } from 'react-native-reanimated';
import { timing } from 'react-native-redash';
import { useToast } from '@etaui/toast/useToast'

const Root = styled.View`
    flex: 1.15;
    flexDirection: column;
    justifyContent: flex-start;
    alignItems: center;
    marginTop: 30px
`;
const MessageContainer = styled.View`
    min-height 100px;
    paddingHorizontal: 30px;
    background-color: transparent;
`;
const FormContainer = styled.View`
    min-height: 150px;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
    alignSelf: center;
    alignContent: center;
    paddingVertical: 10px;
    background-color: transparent;
`;
const ResendContainer = styled.View`
    flex: 0.1;
    justifyContent: center;
    alignItems: center;
    margin: 5px;
    background-color: transparent;
`;
const ResendButton = styled.TouchableOpacity`
    margin-top: 20px;
`;

const validationSchema = yup.object().shape({
    code: yup
        .string()        
        .matches(/^[0-9]*$/, 'Ingrese únicamente números')
        .min(5, 'Código de confirmación debe tener 5 dígitos')
        .max(5, 'Código de confirmación debe tener 5 dígitos')
        .typeError('Ingrese únicamente números')
        .required('Este campo no puede estar vacío')
});

const CodeConfirmationForm = () => {
    const { verifyCode, state } = useContext(Context);
    const themeContext = useContext(ThemeContext);
	const route = useRoute()
    const [ itemModalOpen, setItemModalOpen, toggleModal ] = useModal();
    const { nextScreen, propsrfc, propop, propcellphone, propemail } = route?.params
    const { showToast } = useToast()
    console.log('CodeConfirmationForm props: ', {nextScreen, propsrfc, propop, propcellphone, propemail} );
    
    let seconds = 60
    const [ timer, settimer ] = useState(seconds)
    const [ timerNIP, settimerNIP ] = useState(false)
    // const progress = new Animated.Value(0);
    // useCode(() => set(progress, timing({ duration: 1000 * seconds })), [progress]);

    useEffect(() => {
        let isUnmount = false
        sendNIP()
        return () => {
            isUnmount = true
        }
    }, [])

    useEffect(() => {
        let isUnmount = false
        console.log('[CodeConfirmationForm] state:', state);
        if (state.error === false && state.data.rfc === null) {
            showToast('Success', state.message)
        } else if (state.error === true && state.data.rfc === null){
            showToast('Error', state.message)
            console.log('error true we');
        }

        return () => {
          isUnmount = true
        }
    }, [state])

    const sendNIP = async () => {
        const sendNIP = await axios.get(`${URL}RegistroUsuario/UpdatePassword?numero=${propcellphone}&pass=${1}&op=${1}&Correo=${propemail}`)
        console.log('[sendNIP]');
    }

    const resendNIP = async () => {
        console.log({ propcellphone, propemail });
        let leftTime = seconds
        try {
            if (propcellphone) {
                sendNIP()
                settimerNIP(true)

                let countdown = setInterval(() => {
                    if (leftTime <= 0) {
                        clearInterval(countdown)
                        settimer(seconds)
                        leftTime = seconds + 1
                        settimerNIP(false)
                        console.log('cleared');
                    }

                    leftTime -= 1
                    settimer(leftTime)
                }, 1000);
            }
        } catch (error) {
            console.warn('SignUp CodeConfirmationForm resendNIP error: ', error);
        }
    }

    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()} style={{ flex: 1 }}>
            <Root>
                {/* {
                    state.error ?   
                        <ETAModal
                            text={state.message}
                            isActive={itemModalOpen}
                            handleClose={() => setItemModalOpen(false)}
                            textColor={themeContext.PRIMARY_TEXT_COLOR_LIGHT}
                            textButton='De acuerdo'
                        />
                    :   null
                } */}
                <>
                    <MessageContainer>
                        <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='center' >
                            En unos momentos recibirá un código de verificación por medio de SMS al télefono proporcionado y/o al email indicado, favor de ingresarlo a continuación.
                        </ETASimpleText>
                    </MessageContainer>
                    <FormContainer>
                        <Formik
                            enableReinitialize={true}
                            initialValues={{ 
                                code: ''
                            }}
                            onSubmit={(values, actions) => {
                                console.log('OnSubmit Code', {rfc: propsrfc, code: values.code, op: propop, nextScreen, propcellphone});
                                verifyCode({ rfc: propsrfc, code: values.code, op: propop, nextScreen, propcellphone });
                                toggleModal(true)
                                setTimeout(() => {
                                    actions.setSubmitting(false)
                                }, 2000);
                            }}
                            validationSchema={validationSchema}
                            >
                            {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors }) => (
                                <>
                                    <ETATextInputFilled
                                        value={values.code}
                                        placeholder='Código de confirmación *'
                                        placeholderTextColor='#777'
                                        keyboardType='phone-pad'
                                        autoCapitalize='none'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={5}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={false} //password
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('code')}
                                        // onBlur={handleBlur('code')}
                                        // selection='1, 4'//? no sé we xd
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        errors.code
                                        ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.code}</ETASimpleText>
                                        : null
                                    }
                                    <ETAButtonFilled title='Validar' onPress={handleSubmit} disabled={isSubmitting ? true : false} colorButton={themeContext.PRIMARY_COLOR} padding={isSubmitting ? 10 : 120} />
                                </>
                            )}
                        </Formik>
                    </FormContainer>
                    {
                        timerNIP
                        ?   <ResendContainer>
                                <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : 'bold'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='center' >
                                    NIP reenviado. 
                                </ETASimpleText>
                                <ETASimpleText size={14} weight={Platform.OS === 'ios' ? 'bold' : '500'} color='#9c9c9c' align='left' >
                                    Enviar de nuevo <ETASimpleText size={14} weight={Platform.OS === 'ios' ? 'bold' : 'bold'} color={themeContext.EXITUS_COLOR} align='center' >({timer.toString()})</ETASimpleText>
                                </ETASimpleText>
                                
                            </ResendContainer>
                        :   <ResendButton 
                                onPress={resendNIP}
                            >
                                <ResendContainer>
                                    <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='center' >
                                        ¿No recibió el código de confirmación?
                                    </ETASimpleText>
                                    
                                    <ETASimpleText size={14} weight={Platform.OS === 'ios' ? 'bold' : 'bold'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' >
                                        Enviar de nuevo <ETASimpleText size={14} weight={Platform.OS === 'ios' ? 'bold' : 'bold'} color={themeContext.EXITUS_COLOR} align='center' >{' '}</ETASimpleText>
                                    </ETASimpleText>
                                </ResendContainer>
                            </ResendButton>
                    }
                    <ETAToast />
                </>
            </Root>
        </TouchableWithoutFeedback>
    );
}

export default CodeConfirmationForm;