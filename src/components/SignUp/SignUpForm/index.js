import React, { useState, useEffect, useContext } from 'react';
import { Platform, View, TouchableWithoutFeedback, Text } from 'react-native';
import styled, { ThemeContext } from 'styled-components/native';
import DateTimePicker from '@react-native-community/datetimepicker';
import configAPI from '@context/configAPI';
import { Formik } from 'formik';
import * as yup from 'yup';
import { useNavigation } from '@react-navigation/native';
import { ETASimpleText, ETAPicker, ETACheckBox, ETAButtonFilled, ETATextInputFilled, ETAModal } from '@etaui';
import {useModal} from '@etaui/modal/useModal';
import { Context } from '@context';
import {connect} from 'react-redux';
import { GET_DATA_REQUEST } from '@redux/auth/banks/actions';

const Root = styled.View`
    flex: 1;
    display: flex;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
    width: 100%;
`;
const HeadTextContainer = styled.View`
    margin-top: 20px;
    padding-vertical: 10px;
    background-color: transparent;
`;
const FormContainer = styled.View`
    display: flex;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
    alignContent: center;
    alignSelf: center;
    width: 100%;
    marginVertical: 5px;
`;
const ColumnContainer = styled.View`
    display: flex;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
`;
const DateContainer = styled.View`
`;

const validationSchema = yup.object().shape({
    name: yup
        .string()
        .matches(/^[A-Za-z ]*$/, 'Únicamente ingrese letras sin tilde')
        .required('Este campo no puede estar vacío')
        .uppercase(),
    secondname: yup
        .string()
        .matches(/^[A-Za-z ]*$/, 'Únicamente ingrese letras sin tilde')
        .uppercase(),
    lastname: yup
        .string()
        .matches(/^[A-Za-z ]*$/, 'Únicamente ingrese letras sin tilde')
        .required('Este campo no puede estar vacío')
        .uppercase(),
    secondlastname: yup
        .string()
        .matches(/^[A-Za-z ]*$/, 'Únicamente ingrese letras sin tilde')
        .required('Este campo no puede estar vacío')
        .uppercase(),
    birthdate: yup
        .string()
        .required('Este campo no puede estar vacío'),
    email: yup
        .string()
        .email('Ingrese un email válido')
        .required('Este campo no puede estar vacío'),
    password: yup
        .string()
        .required('Este campo no puede estar vacío'),
    confirmpassword: yup
        .string()
        .oneOf([yup.ref('password')], 'Las contraseñas no coinciden')
        .required('Este campo no puede estar vacío'),
    cellphone: yup
        .string()
        .matches(/^[0-9]*$/, 'Ingrese únicamente números')
        .min(10, 'Teléfono celular debe tener 10 dígitos')
        .max(10, 'Teléfono celular debe tener 10 dígitos')
        .required('Este campo no puede estar vacío')
        .typeError('Ingrese únicamente números'),
    clabeult: yup
        .string()
        .matches(/^[0-9]*$/, 'Ingrese únicamente números')
        .min(4, 'CLABE debe tener 4 dígitos')
        .max(4, 'CLABE debe tener 4 dígitos')
        .required('Este campo no puede estar vacío')
        .typeError('Ingrese únicamente números'),
    bank: yup
        .string()
        .required('Este campo no puede estar vacío'),
    privacy: yup
        .boolean()
        .required('Por favor acepte los Términos y Condiciones')
        .oneOf([true], 'Por favor acepte los Términos y Condiciones')
});

const mapStateToProps = (state, props) => {
	const {data} = state.banks
	return {data}
}

const mapDispatchProps = (dispatch, props) => ({
	getDataRequest: () => {
		dispatch({
			type: GET_DATA_REQUEST,
			payload: {},
		})
	},
})

const SignUpForm = () => {
    const themeContext = useContext(ThemeContext);
    const navigation = useNavigation();
    const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
    const { signUp, state } = useContext(Context);
    const [ banks, setbanks ] = useState([]);

    useEffect(() => {
        let isUnmount = false
        _getBanks()
    }, [])

    const _getBanks = async () => {
        try {
            let { data } = await configAPI.get('Bancos/GetBancos');
            let newBanksArray = [];
            if (data) {
                data.forEach(element => {
                    newBanksArray.push(
                        {
                            'key': element.NombreCorto,
                            'label': element.NombreCorto,
                            'value': element.NombreCorto
                        }
                    )
                });
                // console.log('newBanksArray:', newBanksArray);
                    setbanks(newBanksArray);   
            }
        } catch (error) {
            console.log('getBanks error: ', error);
        }
    }

/* Datapicker code */
    const [ date, setDate ] = useState('');
    const [ mode, setMode ] = useState('date');
    const [ show, setShow ] = useState(false);
    const [ birthdateText, setbirthdateText ] = useState('Fecha de nacimiento *');
  
    useEffect(() => {
        let isUnmount = false
        const today = new Date();
        let yearconvertedewe = today.getFullYear();
        let monthconvertedewe = ('0' + (today.getMonth() + 1)).slice(-2)
        let dayconvertedewe = today.getDate()
        const eighteenyearsago = toTimestamp(yearconvertedewe - 18, monthconvertedewe, Platform.OS === 'ios' ? dayconvertedewe : dayconvertedewe + 1, 0,0,0)
        // console.log('datepicker 18+', new Date(eighteenyearsago));
        let dateplus18 = new Date(eighteenyearsago)
        let formated = `${('0' + (Platform.OS == 'ios' ? dateplus18.getDate() + 1 : dateplus18.getDate())).slice(-2)}/${dateplus18.getMonth() + 1}/${dateplus18.getFullYear()}` 
        // console.log('dateplus18', eighteenyearsago);
      
        setDate(eighteenyearsago)
        return () => {
          isUnmount = true
        }
    }, [])

    let datetime = new Date();
    let year = datetime.getFullYear();
    let month = datetime.getMonth();;
    let day = datetime.getDate();

    const _onChange = (selectedDate) => {
        // console.log('///////////////////////////////////////////');
        // console.log('selectedDate', selectedDate);
        
        const currentDate = selectedDate;
        setShow(Platform.OS === 'ios');
        
        let yearconverted = currentDate.getFullYear();
        let monthconverted = ('0' + (currentDate.getMonth() + 1)).slice(-2)
        let dayconverted = ('0' + (Platform.OS == 'ios' ? currentDate.getDate() + 1 : currentDate.getDate())).slice(-2)
        const datechanged = toTimestamp(yearconverted, monthconverted, Platform.OS === 'ios' ? dayconverted : dayconverted + 1, 0,0,0)
        var _completeDate = `${yearconverted}-${monthconverted}-${dayconverted}`;
        // console.log('nwn', _completeDate);
        // console.log('uwu', datechanged);
        setDate(datechanged);
        setbirthdateText(_completeDate)

        // return _completeDate;
    };

    const showMode = currentMode => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const toTimestamp = ( year,month,day,hour,minute,second ) => {
        var datum = new Date(Date.UTC( year,month-1,day,hour,minute,second ));
        
        return datum.getTime();
    } 
/* /Datapicker code */

    return (
        <Root>
            {
                state.data.error 
                ?   <ETAModal
                        // text='Por favor corrobora tus datos e intentalo de nuevo.'
                        text={state.message.charAt(0).toUpperCase() + state.message.toLowerCase().slice(1)}
                        isActive={itemModalOpen}
                        handleClose={() => setItemModalOpen(false)}
                        textColor={themeContext.PRIMARY_TEXT_COLOR_LIGHT}
                        textButton='De acuerdo'
                    />
                :   null
            }
            <HeadTextContainer>
                <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.SECONDARY_BACKGROUND_COLOR} align='left' >Campos obligatorios *</ETASimpleText>
            </HeadTextContainer>
            <FormContainer>
                <Formik
                    enableReinitialize={true}
                    initialValues={{ 
                        name: '',
                        secondname: '',
                        lastname: '',
                        secondlastname: '',
                        birthdate: '',
                        email: '',
                        password: '',
                        confirmpassword: '',
                        cellphone: '',
                        clabeult: '',
                        bank: '',
                        privacy: false
                    }}
                    onSubmit={(values, actions) => {
                        signUp({
                            name: values.name.toUpperCase(),
                            secondname: values.secondname.toUpperCase(),
                            lastname: values.lastname.toUpperCase(),
                            secondlastname: values.secondlastname.toUpperCase(),
                            birthdate: values.birthdate,
                            email: values.email,
                            // password: values.password.toUpperCase(),
                            password: values.password,
                            // confirmpassword: values.confirmpassword.toUpperCase(),
                            confirmpassword: values.confirmpassword,
                            cellphone: values.cellphone,
                            clabeult: values.clabeult,
                            bank: values.bank,
                            privacy: values.privacy
                        });
                        setTimeout(() => {
                            toggleModal(state.data.error);
                            actions.setSubmitting(false)
                        }, 2000);

                        // actions.setSubmitting(!state.data.error)
                        // setTimeout(() => {
                        // }, 2000);
                        // if (state.message) {
                        // }
                    }}
                    validationSchema={validationSchema}
                    >
                    {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors, touched, isValid, setFieldValue }) => (
                        <>
                            <ColumnContainer>
                                <ETATextInputFilled
                                    value={values.name}
                                    placeholder='Nombre *'
                                    placeholderTextColor='#777'
                                    keyboardType='default'
                                    autoCapitalize='characters'
                                    allowFontScaling={true}
                                    autoCorrect={true}
                                    autoFocus={true}
                                    blurOnSubmit={false}
                                    caretHidden={false}
                                    clearButtonMode='while-editing'
                                    contextMenuHidden={false}
                                    editable={true}
                                    enablesReturnKeyAutomatically={false}
                                    underlineColorAndroid='transparent'
                                    keyboardAppearance='dark'
                                    maxLength={100}
                                    multiline={false}
                                    numberOfLines={1} //android
                                    returnKeyLabel='next' //android
                                    secureTextEntry={false} //password
                                    spellCheck={true}
                                    textContentType='none'
                                    returnKeyType='next'
                                    textsize={15}
                                    height={40}
                                    width={275}
                                    onChangeText={handleChange('name')}
                                    onBlur={handleBlur('name')}
                                    // selection='1, 4'// pendiente de conocer
                                    // onBlur={text => this._onBlur(text)}
                                    // onChangeText={onchangetext}
                                    // onEndEditing={text => this._onEndEditing(text)}
                                    // onFocus={text => this._onFocus(text)}
                                    // ref={(input) => {this.emailInput = input }}
                                    // onKeyPress={}
                                    // onScroll={}
                                />
                                {
                                    errors.name
                                    ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.name}</ETASimpleText>
                                    : null
                                }
                                <ETATextInputFilled
                                    value={values.secondname}
                                    placeholder='Segundo nombre'
                                    placeholderTextColor='#777'
                                    keyboardType='default'
                                    autoCapitalize='characters'
                                    allowFontScaling={true}
                                    autoCorrect={true}
                                    autoFocus={true}
                                    blurOnSubmit={false}
                                    caretHidden={false}
                                    clearButtonMode='while-editing'
                                    contextMenuHidden={false}
                                    editable={true}
                                    enablesReturnKeyAutomatically={false}
                                    underlineColorAndroid='transparent'
                                    keyboardAppearance='dark'
                                    maxLength={100}
                                    multiline={false}
                                    numberOfLines={1} //android
                                    returnKeyLabel='next' //android
                                    secureTextEntry={false} //password
                                    spellCheck={true}
                                    textContentType='none'
                                    returnKeyType='next'
                                    textsize={15}
                                    height={40}
                                    width={275}
                                    onChangeText={handleChange('secondname')}
                                    onBlur={handleBlur('secondname')}
                                    // selection='1, 4'// pendiente de conocer
                                    // onBlur={text => this._onBlur(text)}
                                    // onChangeText={onchangetext}
                                    // onEndEditing={text => this._onEndEditing(text)}
                                    // onFocus={text => this._onFocus(text)}
                                    // ref={(input) => {this.emailInput = input }}
                                    // onKeyPress={}
                                    // onScroll={}
                                />
                                {
                                    errors.secondname
                                    ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.secondname}</ETASimpleText>
                                    : null
                                }
                            </ColumnContainer>
                            <ColumnContainer>
                                <ETATextInputFilled
                                    value={values.lastname}
                                    placeholder='Apellido paterno *'
                                    placeholderTextColor='#777'
                                    keyboardType='default'
                                    autoCapitalize='characters'
                                    allowFontScaling={true}
                                    autoCorrect={true}
                                    autoFocus={true}
                                    blurOnSubmit={false}
                                    caretHidden={false}
                                    clearButtonMode='while-editing'
                                    contextMenuHidden={false}
                                    editable={true}
                                    enablesReturnKeyAutomatically={false}
                                    underlineColorAndroid='transparent'
                                    keyboardAppearance='dark'
                                    maxLength={100}
                                    multiline={false}
                                    numberOfLines={1} //android
                                    returnKeyLabel='next' //android
                                    secureTextEntry={false} //password
                                    spellCheck={true}
                                    textContentType='none'
                                    returnKeyType='next'
                                    textsize={15}
                                    height={40}
                                    width={275}
                                    onChangeText={handleChange('lastname')}
                                    onBlur={handleBlur('lastname')}
                                    // selection='1, 4'// pendiente de conocer
                                    // onBlur={text => this._onBlur(text)}
                                    // onChangeText={onchangetext}
                                    // onEndEditing={text => this._onEndEditing(text)}
                                    // onFocus={text => this._onFocus(text)}
                                    // ref={(input) => {this.emailInput = input }}
                                    // onKeyPress={}
                                    // onScroll={}
                                />
                                {
                                    errors.lastname
                                    ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.lastname}</ETASimpleText>
                                    : null
                                }
                                <ETATextInputFilled
                                    value={values.secondlastname}
                                    placeholder='Apellido materno *'
                                    placeholderTextColor='#777'
                                    keyboardType='default'
                                    autoCapitalize='characters'
                                    allowFontScaling={true}
                                    autoCorrect={true}
                                    autoFocus={true}
                                    blurOnSubmit={false}
                                    caretHidden={false}
                                    clearButtonMode='while-editing'
                                    contextMenuHidden={false}
                                    editable={true}
                                    enablesReturnKeyAutomatically={false}
                                    underlineColorAndroid='transparent'
                                    keyboardAppearance='dark'
                                    maxLength={100}
                                    multiline={false}
                                    numberOfLines={1} //android
                                    returnKeyLabel='next' //android
                                    secureTextEntry={false} //password
                                    spellCheck={true}
                                    textContentType='none'
                                    returnKeyType='next'
                                    textsize={15}
                                    height={40}
                                    width={275}
                                    onChangeText={handleChange('secondlastname')}
                                    onBlur={handleBlur('secondlastname')}
                                    // selection='1, 4'// pendiente de conocer
                                    // onBlur={text => this._onBlur(text)}
                                    // onChangeText={onchangetext}
                                    // onEndEditing={text => this._onEndEditing(text)}
                                    // onFocus={text => this._onFocus(text)}
                                    // ref={(input) => {this.emailInput = input }}
                                    // onKeyPress={}
                                    // onScroll={}
                                />
                                {
                                    errors.secondlastname
                                    ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.secondlastname}</ETASimpleText>
                                    : null
                                }
                            </ColumnContainer>
                            <ColumnContainer>
                                <DateContainer>
                                    <View style={{
                                        backgroundColor: themeContext.THIRD_BACKGROUND_COLOR_LIGHT,
                                        borderRadius: 30,
                                        width: 275,
                                        height: 45,
                                        margin: 5,
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                    }}>
                                        <TouchableWithoutFeedback onPress={showDatepicker}>
                                            <Text style={{ color: '#777', fontSize: 15, marginLeft: 15, fontWeight:'400'}} >{birthdateText.toString()}</Text>
                                        </TouchableWithoutFeedback>
                                    </View>
                                    {show && (
                                        <DateTimePicker
                                            // value={new Date(values.birthdate)}
                                            testID='dateTimePicker'
                                            locale='es-ES'
                                            timeZoneOffsetInMinutes={0}
                                            value={new Date(date)}
                                            mode={mode}
                                            is24Hour={!true}
                                            display='spinner'
                                            textColor='#777'
                                            onChange={(event, value) => {setFieldValue('birthdate', value); _onChange(value)}}
                                            maximumDate={new Date(year-18, month, Platform.OS === 'ios'  ? day : day)}
                                        />
                                    )}
                                </DateContainer>
                                {
                                    errors.birthdate
                                    ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.birthdate}</ETASimpleText>
                                    : null
                                }
                            </ColumnContainer>
                                <ETATextInputFilled
                                    value={values.email}
                                    placeholder='Email *'
                                    placeholderTextColor='#777'
                                    keyboardType='email-address'
                                    autoCapitalize='none'
                                    allowFontScaling={true}
                                    autoCorrect={true}
                                    autoFocus={true}
                                    blurOnSubmit={false}
                                    caretHidden={false}
                                    clearButtonMode='while-editing'
                                    contextMenuHidden={false}
                                    editable={true}
                                    enablesReturnKeyAutomatically={false}
                                    underlineColorAndroid='transparent'
                                    keyboardAppearance='dark'
                                    maxLength={100}
                                    multiline={false}
                                    numberOfLines={1} //android
                                    returnKeyLabel='next' //android
                                    secureTextEntry={false} //password
                                    spellCheck={true}
                                    textContentType='none'
                                    returnKeyType='next'
                                    textsize={15}
                                    height={40}
                                    width={275}
                                    onChangeText={handleChange('email')}
                                    onBlur={handleBlur('email')}
                                    // selection='1, 4'// pendiente de conocer
                                    // onBlur={text => this._onBlur(text)}
                                    // onChangeText={onchangetext}
                                    // onEndEditing={text => this._onEndEditing(text)}
                                    // onFocus={text => this._onFocus(text)}
                                    // ref={(input) => {this.emailInput = input }}
                                    // onKeyPress={}
                                    // onScroll={}
                                />
                                {
                                    errors.email
                                    ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.email}</ETASimpleText>
                                    : null
                                }
                            <ColumnContainer>
                                <ETATextInputFilled
                                    value={values.password}
                                    placeholder='Contraseña *'
                                    placeholderTextColor='#777'
                                    keyboardType='default'
                                    autoCapitalize='characters'
                                    allowFontScaling={true}
                                    autoCorrect={true}
                                    autoFocus={true}
                                    blurOnSubmit={false}
                                    caretHidden={false}
                                    clearButtonMode='while-editing'
                                    contextMenuHidden={false}
                                    editable={true}
                                    enablesReturnKeyAutomatically={false}
                                    underlineColorAndroid='transparent'
                                    keyboardAppearance='dark'
                                    maxLength={100}
                                    multiline={false}
                                    numberOfLines={1} //android
                                    returnKeyLabel='next' //android
                                    secureTextEntry={!true} //password
                                    spellCheck={true}
                                    textContentType='none'
                                    returnKeyType='next'
                                    textsize={15}
                                    height={40}
                                    width={275}
                                    onChangeText={handleChange('password')}
                                    onBlur={handleBlur('password')}
                                    // selection='1, 4'// pendiente de conocer
                                    // onBlur={text => this._onBlur(text)}
                                    // onChangeText={onchangetext}
                                    // onEndEditing={text => this._onEndEditing(text)}
                                    // onFocus={text => this._onFocus(text)}
                                    // ref={(input) => {this.emailInput = input }}
                                    // onKeyPress={}
                                    // onScroll={}
                                />
                                {
                                    errors.password
                                    ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.password}</ETASimpleText>
                                    : null
                                }
                                <ETATextInputFilled
                                    value={values.confirmpassword}
                                    placeholder='Repetir contraseña *'
                                    placeholderTextColor='#777'
                                    keyboardType='default'
                                    autoCapitalize='characters'
                                    allowFontScaling={true}
                                    autoCorrect={true}
                                    autoFocus={true}
                                    blurOnSubmit={false}
                                    caretHidden={false}
                                    clearButtonMode='while-editing'
                                    contextMenuHidden={false}
                                    editable={true}
                                    enablesReturnKeyAutomatically={false}
                                    underlineColorAndroid='transparent'
                                    keyboardAppearance='dark'
                                    maxLength={100}
                                    multiline={false}
                                    numberOfLines={1} //android
                                    returnKeyLabel='next' //android
                                    secureTextEntry={!true} //password
                                    spellCheck={true}
                                    textContentType='none'
                                    returnKeyType='next'
                                    textsize={15}
                                    height={40}
                                    width={275}
                                    onChangeText={handleChange('confirmpassword')}
                                    onBlur={handleBlur('confirmpassword')}
                                    // selection='1, 4'// pendiente de conocer
                                    // onBlur={text => this._onBlur(text)}
                                    // onChangeText={onchangetext}
                                    // onEndEditing={text => this._onEndEditing(text)}
                                    // onFocus={text => this._onFocus(text)}
                                    // ref={(input) => {this.emailInput = input }}
                                    // onKeyPress={}
                                    // onScroll={}
                                />
                                {
                                    touched.confirmpassword && errors.confirmpassword
                                    ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.confirmpassword}</ETASimpleText>
                                    : null
                                }
                            </ColumnContainer>
                            <ColumnContainer>
                                <ETATextInputFilled
                                    value={values.cellphone}
                                    placeholder='Teléfono celular *'
                                    placeholderTextColor='#777'
                                    keyboardType='phone-pad'
                                    autoCapitalize='none'
                                    allowFontScaling={true}
                                    autoCorrect={true}
                                    autoFocus={true}
                                    blurOnSubmit={false}
                                    caretHidden={false}
                                    clearButtonMode='while-editing'
                                    contextMenuHidden={false}
                                    editable={true}
                                    enablesReturnKeyAutomatically={false}
                                    underlineColorAndroid='transparent'
                                    keyboardAppearance='dark'
                                    maxLength={10}
                                    multiline={false}
                                    numberOfLines={1} //android
                                    returnKeyLabel='next' //android
                                    secureTextEntry={false} //cellphone
                                    spellCheck={true}
                                    textContentType='none'
                                    returnKeyType='next'
                                    textsize={15}
                                    height={40}
                                    width={275}
                                    onChangeText={handleChange('cellphone')}
                                    onBlur={handleBlur('cellphone')}
                                    // selection='1, 4'// pendiente de conocer
                                    // onBlur={text => this._onBlur(text)}
                                    // onChangeText={onchangetext}
                                    // onEndEditing={text => this._onEndEditing(text)}
                                    // onFocus={text => this._onFocus(text)}
                                    // ref={(input) => {this.emailInput = input }}
                                    // onKeyPress={}
                                    // onScroll={}
                                />
                                {
                                    errors.cellphone
                                    ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.cellphone}</ETASimpleText>
                                    : null
                                }
                                <ETATextInputFilled
                                    value={values.clabeult}
                                    placeholder='CLABE (últimos 4 dígitos) *'
                                    placeholderTextColor='#777'
                                    keyboardType='number-pad'
                                    autoCapitalize='none'
                                    allowFontScaling={true}
                                    autoCorrect={true}
                                    autoFocus={true}
                                    blurOnSubmit={false}
                                    caretHidden={false}
                                    clearButtonMode='while-editing'
                                    contextMenuHidden={false}
                                    editable={true}
                                    enablesReturnKeyAutomatically={false}
                                    underlineColorAndroid='transparent'
                                    keyboardAppearance='dark'
                                    maxLength={4}
                                    multiline={false}
                                    numberOfLines={1} //android
                                    returnKeyLabel='next' //android
                                    secureTextEntry={false} //password
                                    spellCheck={true}
                                    textContentType='none'
                                    returnKeyType='next'
                                    textsize={15}
                                    height={40}
                                    width={275}
                                    onChangeText={handleChange('clabeult')}
                                    onBlur={handleBlur('clabeult')}
                                    // selection='1, 4'// pendiente de conocer
                                    // onBlur={text => this._onBlur(text)}
                                    // onChangeText={onchangetext}
                                    // onEndEditing={text => this._onEndEditing(text)}
                                    // onFocus={text => this._onFocus(text)}
                                    // ref={(input) => {this.emailInput = input }}
                                    // onKeyPress={}
                                    // onScroll={}
                                />
                                {
                                    errors.clabeult
                                    ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.clabeult}</ETASimpleText>
                                    : null
                                }
                            </ColumnContainer>
                            <ETAPicker 
                                items={banks} 
                                placeholder='Seleccione un banco *'
                                onChange={handleChange('bank')}
                                selected={values.bank}
                            />
                            {
                                errors.bank
                                ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.bank}</ETASimpleText>
                                : null
                            }
                            <ETACheckBox 
                                title='Acepto Términos y Condiciones'
                                checkedTitle='Términos y Condiciones aceptados'
                                onChange={() => setFieldValue('privacy', !values.privacy)}
                                onPressTitle={() => navigation.navigate('NoticeOfPrivacyScreen')}
                                checked={values.privacy ? true : false}
                                // checked={values.privacy}
                                color={themeContext.EXITUS_COLOR} />
                                {
                                    errors.privacy
                                    ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.privacy}</ETASimpleText>
                                    : null
                                }
                                <ETAButtonFilled title='Enviar' onPress={handleSubmit} disabled={isSubmitting} colorButton={themeContext.PRIMARY_COLOR} padding={isSubmitting ? 10 : 120} 
                            />
                        </>
                    )}
                </Formik>
            </FormContainer>
        </Root>
    );
}

const SignUpFormConnect = connect(
	mapStateToProps,
	mapDispatchProps,
)(SignUpForm)

export default SignUpFormConnect