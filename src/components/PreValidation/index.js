import React, {useEffect, useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/native';
import { Platform, Dimensions, useColorScheme } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { ETASimpleText, ETAButtonFilled } from '@etaui';

const { width } = Dimensions.get('window')

const Root = styled.View`
    flex: 1;
    width: ${width - 50}px;
    justifyContent: center;
    alignItems: center;
`;
const NIPMessageContainer = styled.View`
    min-height: 250px;
    justifyContent: center;
    alignItems: center;
    padding-horizontal: 20px;
    padding-vertical: 20px;
    border-radius: 10px;
    background-color: white;
`;
const HeaderLogo = styled.Image`
    margin-bottom: 30px
`;

const PreValidation = () => {
    const themeContext = useContext(ThemeContext);
    const navigation = useNavigation();
    const colorSchema = useColorScheme();
	const route = useRoute();
    const { propsrfc, propop, propcellphone } = route?.params
    console.log('PreValidation: ', {propsrfc, propop, propcellphone});

    return (
        <Root>
            <HeaderLogo 
                style={{
                    height: Platform.OS === 'ios' ? 30 : 30,
                    width: Platform.OS === 'ios' ? 140 : 140 }}
                source={colorSchema == 'dark' ? require('@assets/icons/app-icon.png') : require('@assets/icons/app-icon.png')}
            />
            <NIPMessageContainer>
                <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='center' >
                    Su registro ha quedado inconcluso, para terminarlo deberá validar el código de confirmación y luego intentar iniciar sesión de nuevo.
                </ETASimpleText>
                <ETAButtonFilled title='Entendido' onPress={() => navigation.navigate('CodeConfirmationScreen', { nextScreen: 'LoginScreen', propsrfc, propop, propcellphone, propemail: '' }) } disabled={false} colorButton={themeContext.PRIMARY_COLOR} padding={105} />
                {/* <ETAButtonFilled title='Entendido' onPress={() => alert(`[PreValidation] params data: ${propsrfc}, ${propop}, ${propcellphone}`) } disabled={false} colorButton={themeContext.PRIMARY_COLOR} padding={105} /> */}
            </NIPMessageContainer>
        </Root>
    );
}

export default PreValidation;