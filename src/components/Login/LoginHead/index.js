import React, { useEffect, useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/native';
import { Platform, useColorScheme } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import { ETASimpleText } from '@etaui';
import { vAndroid, vIOS } from '@utils/constants';

const HeaderContainer = styled.View`
    min-height: 200px;
    justifyContent: center;
    alignItems: center;
    margin-top: 10px;
    background-color: transparent;
`;
const HeaderLogo = styled.Image`
    marginVertical: 5px
`;

const LoginHead = () => {
    const themeContext = useContext(ThemeContext);
    const isFocused = useIsFocused();
    const colorSchema = useColorScheme();

    useEffect(() => {
        let isUnmount = false
        if (isFocused) {
        }
        return () => {
          isUnmount = true
        }
    }, [isFocused]);
    
    return (
        <>
            <HeaderContainer>
                <HeaderLogo 
                    style={{
                        height: Platform.OS === 'ios' ? 52 : 60,
                        width: Platform.OS === 'ios' ? 125 : 145 }}
                    source={colorSchema == 'dark' ? require('../../../../assets/renovapng.png') : require('../../../../assets/renovapng.png')}
                    resizeMode={'cover'}
                />
                <HeaderLogo 
                    style={{
                        height: Platform.OS === 'ios' ? 30 : 25,
                        width: Platform.OS === 'ios' ? 120 : 100,
                        resizeMode: 'cover' }}
                    source={colorSchema == 'dark' ? require('../../../../assets/icons/app-icon.png') : require('../../../../assets/icons/app-icon.png')}
                    
                />
                {/* <ExitusSVGIcon/> */}
                {/* <ETASimpleText size={17} weight='bold' color={themeContext.SECONDARY_BACKGROUND_COLOR} align='left' >Exitus Renova</ETASimpleText> */}
                <ETASimpleText size={13} weight={Platform.OS === 'ios' ? '400' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' >v{Platform.OS === 'ios' ? vIOS : vAndroid}</ETASimpleText>
            </HeaderContainer>
        </>
    );
}

export default LoginHead;