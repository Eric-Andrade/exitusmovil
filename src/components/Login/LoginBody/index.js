import React, { useContext } from 'react';
import styled, { ThemeContext } from 'styled-components/native';
import { Platform } from 'react-native';
import { AntDesign } from '@icons';
import { useNavigation } from '@react-navigation/native';
import { ETASimpleText, ETALink } from '@etaui';
import { FovisssteSvgIcon, IsssteSvgIcon, ImssSvgIcon, PemexSVGIcon } from '@icons'

const Root = styled.View`
    min-height: 200px;
    justifyContent: center;
    alignItems: center;
    width: 100%;
    background-color: transparent;
`;
const Container = styled.View`    
    flex: 0.9;
    justifyContent: flex-start;
    alignItems: center;
    width: 100%;
`;
const ContactContainer = styled.View`
    flexDirection: column;
    width: 100%;
    justifyContent: center;
    alignItems: center;
    margin-top: 25px;
    background-color: transparent;
`;
const ContactTextContainer = styled.View`
    flexDirection: row;
    justifyContent: center;
    alignItems: center;
    alignSelf: center;
    marginVertical: 7px;
    background-color: transparent;
`;
const ContactCellContainer = styled.View`
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
    alignSelf: center;
    marginVertical: 7px;
`;
const ContactIconContainer = styled.View`
    marginHorizontal: 5px;
`;
const ForgerPassContainer = styled.View`
    marginVertical: 5px;
    justifyContent: center;
    alignItems: center;
`;
const SignUpLinkContainer = styled.View`
    marginVertical: 5px;
    justifyContent: center;
    alignItems: center;
    `;
const AllianceContainer = styled.View`
    flex: 0.5;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
    background-color: transparent
`;
const AllianceImagesContainer = styled.View`
    flex: 0.3;
    flexDirection: row;
    marginTop: 20px;
    justifyContent: center;
    alignItems: center;
`;
const AllianceImageContainer = styled.View`
    height: 30px;
    width: 15px;
    marginHorizontal: 17px;
    resizeMode: center;
    alignItems: center;
`;

const Link = styled.TouchableOpacity``;

const LoginBody = () => {
    const themeContext = useContext(ThemeContext);
    const navigation = useNavigation();

    return (
        <Root>
            <Container>
                <Link 
                    onPress={() => navigation.navigate('RecoveryPassScreen')}>
                    <ForgerPassContainer>
                        <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '300' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' >¿Olvidó su contraseña? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '700' : '700'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='center' >Recuperela aquí</ETASimpleText></ETASimpleText>
                    </ForgerPassContainer>
                </Link>
                <Link 
                    onPress={() => navigation.navigate('SignUpScreen')}>
                    <SignUpLinkContainer>
                        <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '300' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' >¿Sin cuenta? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '700' : '700'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='center' >Registrese aquí</ETASimpleText></ETASimpleText>
                    </SignUpLinkContainer>
                </Link>
                {/* <Link 
                    onPress={() => navigation.navigate('PreValidationScreen', { propsrfc: 'DSAD767687DASDJM', propop: 1, propcellphone: '1234567890' })}>
                    <SignUpLinkContainer>
                        <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '300' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' >Cuenta inconclusa</ETASimpleText>
                    </SignUpLinkContainer>
                </Link> */}
                <ContactContainer> 
                    <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '300' : '500'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='left' >¿Necesita ayuda para realizar su renovación?</ETASimpleText>
                    <ContactTextContainer>
                        <ContactIconContainer>
                            <AntDesign name='phone' size={20} color={themeContext.EXITUS_COLOR} />
                        </ContactIconContainer>
                        <ContactCellContainer>
                            <ETALink url='tel:5544408080' text='Llámenos al 55 4440 8080' size={15} weight={Platform.OS === 'ios' ? '600' : '700'} color={themeContext.EXITUS_COLOR} align='left' />
                        </ContactCellContainer>
                    </ContactTextContainer>
                </ContactContainer>
                <AllianceContainer>
                    <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '500' : '700'} color={themeContext.PRIMARY_TEXT_COLOR_LIGHT} align='center' >Alianzas y Convenios</ETASimpleText>
                    <AllianceImagesContainer>
                        <AllianceImageContainer>
                            <IsssteSvgIcon 
                                size={30}
                                color={themeContext.PRIMARY_TEXT_COLOR_LIGHT}/>
                        </AllianceImageContainer>
                        
                        <AllianceImageContainer>
                            <ImssSvgIcon 
                                size={30}
                                color={themeContext.PRIMARY_TEXT_COLOR_LIGHT}/>
                        </AllianceImageContainer>
                        
                        <AllianceImageContainer>
                            <PemexSVGIcon 
                                size={50}
                                color={themeContext.PRIMARY_TEXT_COLOR_LIGHT}/>
                        </AllianceImageContainer>

                        <AllianceImageContainer>
                            <FovisssteSvgIcon 
                                size={30}
                                color={themeContext.PRIMARY_TEXT_COLOR_LIGHT}/>
                        </AllianceImageContainer>
                        
                    </AllianceImagesContainer>
                </AllianceContainer>
            </Container>
        </Root>  
    );
}

export default LoginBody;