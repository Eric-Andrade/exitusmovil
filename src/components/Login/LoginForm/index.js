import React, {  useState, useContext, useEffect, memo } from 'react';
import styled, { ThemeContext } from 'styled-components/native';
import { Platform, KeyboardAvoidingView } from 'react-native';
import { Formik } from 'formik';
import * as yup from 'yup';
import { Entypo } from '@icons';
import { ETAButtonFilled, ETATextInputFilled, ETASimpleText, ETAModal } from '@etaui';
import LoginBody from '../LoginBody';
import {useModal} from '@etaui/modal/useModal';
import { Context } from '@context';

const Root = styled.View`
    min-height: 200px;
    flexDirection: column;
    justifyContent: flex-end;
    alignItems: center;
    background-color: transparent;
`;
const FormContainer = styled.View`
    min-height: 200px;
    display: flex;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
`;
const TextInputIcon = styled.TouchableOpacity`
`;

const validationSchema = yup.object().shape({
    cellphone: yup
        .string()        
        .matches(/^[0-9]*$/, 'Ingrese únicamente números')
        .min(10, 'Teléfono celular debe tener 10 dígitos')
        .max(10, 'Teléfono celular debe tener 10 dígitos')
        .typeError('Ingrese únicamente números')
        .required('Este campo no puede estar vacío'),
    password: yup
        .string()
        .required('Este campo no puede estar vacío')
        .uppercase(),
});

const LoginForm = memo(() => {
    const { logIn, state } = useContext(Context);
    const themeContext = useContext(ThemeContext);
    const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
    const [ toogleEye, settoogleEye ] = useState(true);
    const [ mysecureTextEntry, mysetSecureTextEntry ] = useState(true);
    
    const onPassPress = () => {
      mysetSecureTextEntry(!mysecureTextEntry);
      settoogleEye(!toogleEye)
    };

    useEffect(() => {
        let isUnmount = false
        console.log('[LoginForm] state.data: ', state.data );
        if (state.data.error) {
            toggleModal(true);
        }
        return () => {
          isUnmount = true
        }
    }, [state])

    return (
        <Root>
            {
                state.data.error === true && state.data.rfc !== null
                ?   <ETAModal
                        text='Por favor corrobore sus datos e inténtelo de nuevo.'
                        isActive={itemModalOpen}
                        handleClose={() => setItemModalOpen(false)}
                        textColor={themeContext.PRIMARY_TEXT_COLOR_LIGHT}
                        textButton='De acuerdo'
                    />
                :   null
            }
            <KeyboardAvoidingView behavior='padding'>
                <FormContainer>
                        <Formik
                            enableReinitialize={true}
                            initialValues={{
                                cellphone: '', // 5528971070 / 7228733731 /5620222699
                                password: '' // Exitus21 / / exitus2020
                            }}
                            onSubmit={(values, actions) => {
                                logIn({ cellphone: values.cellphone, password: values.password });
                                
                                setTimeout(() => {
                                    // console.log('loading we---', state.data);
                                    actions.setSubmitting(false)
                                }, 3000);
                            }}
                            validationSchema={validationSchema}
                            >
                            {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors }) => (
                                <>
                                    <ETATextInputFilled
                                        value={values.cellphone}
                                        placeholder='Teléfono celular'
                                        placeholderTextColor='#777'
                                        keyboardType='phone-pad'
                                        autoCapitalize='none'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={true}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={10}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={false} //password
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='next'
                                        textsize={15}
                                        height={40}
                                        width={275}
                                        onChangeText={handleChange('cellphone')}
                                        onBlur={handleBlur('cellphone')}
                                        // selection='1, 4'//? no sé we xd
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                    />
                                    {
                                        errors.cellphone
                                        ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '300' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.cellphone}</ETASimpleText>
                                        : null
                                    }
                                    <ETATextInputFilled
                                        value={values.password}
                                        placeholder='Contraseña'
                                        placeholderTextColor='#777'
                                        keyboardType='default'
                                        autoCapitalize='characters'
                                        allowFontScaling={true}
                                        autoCorrect={true}
                                        autoFocus={false}
                                        blurOnSubmit={false}
                                        caretHidden={false}
                                        clearButtonMode='while-editing'
                                        contextMenuHidden={false}
                                        editable={true}
                                        enablesReturnKeyAutomatically={false}
                                        underlineColorAndroid='transparent'
                                        keyboardAppearance='dark'
                                        maxLength={100}
                                        multiline={false}
                                        numberOfLines={1} //android
                                        returnKeyLabel='next' //android
                                        secureTextEntry={mysecureTextEntry} //password
                                        spellCheck={true}
                                        textContentType='none'
                                        returnKeyType='none'
                                        textsize={16}
                                        height={40}
                                        width={275}                        
                                        onChangeText={handleChange('password')}
                                        onBlur={handleBlur('password')}
                                        rightIcon={(
                                            <TextInputIcon onPress={() => onPassPress()}>
                                                <Entypo style={{ color: '#777', marginRight: 65 }} name={ toogleEye ? 'eye' : 'eye-with-line'} size={20} />
                                            </TextInputIcon>
                                        )}
                                        // selection='1, 4'//? no sé we xd
                                        // onBlur={text => this._onBlur(text)}
                                        // onChangeText={onchangetext}
                                        // onEndEditing={text => this._onEndEditing(text)}
                                        // onFocus={text => this._onFocus(text)}
                                        // ref={(input) => {this.emailInput = input }}
                                        // onKeyPress={}
                                        // onScroll={}
                                        paddingHorizontal={60}
                                    />
                                    {
                                        errors.password
                                        ? <ETASimpleText size={14} weight={Platform.OS === 'ios' ? '300' : '500'} color={themeContext.EXITUS_COLOR} align='left' >{errors.password}</ETASimpleText>
                                        : null
                                    }
                                    <ETAButtonFilled title={isSubmitting ? 'Enviando...' : 'Ingresar' } onPress={handleSubmit} disabled={isSubmitting ? true : false} colorButton={themeContext.PRIMARY_COLOR} padding={isSubmitting ? 10 : 115} />
                                </>
                            )}
                        </Formik>
                    <LoginBody />
                </FormContainer>
            </KeyboardAvoidingView>
        </Root>
    );
})

export default LoginForm;