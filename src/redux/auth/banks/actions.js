const GET_DATA_REQUEST = 'banks/GET_DATA_REQUEST'
const GET_DATA_REQUEST_SUCCESS = 'banks/GET_DATA_REQUEST_SUCCESS'
const GET_DATA_REQUEST_FAILED = 'banks/GET_DATA_REQUEST_FAILED'

export {
    GET_DATA_REQUEST,
    GET_DATA_REQUEST_SUCCESS,
    GET_DATA_REQUEST_FAILED
}