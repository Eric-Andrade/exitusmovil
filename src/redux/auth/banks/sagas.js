import { takeEvery, put, call } from 'redux-saga/effects'
import { GET_DATA_REQUEST, GET_DATA_REQUEST_SUCCESS } from './actions'
import { queryApi } from '../../query-api'

function* handler() {
  yield takeEvery(GET_DATA_REQUEST, getDataRequest)
}

function* getDataRequest(action) {
  try {
    const data = yield call(queryApi, {
      endpoint: 'Bancos/GetBancos',
      method: 'GET',
    })
    yield put({
      type: GET_DATA_REQUEST_SUCCESS,
      payload: {
        data: data,
      },
    })
  } catch (error) {
    console.log('Banks error: ', error)
  }
}

export { handler }
