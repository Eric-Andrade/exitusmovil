const GET_DATA_REQUEST = 'updatepassword/GET_DATA_REQUEST'
const GET_DATA_REQUEST_SUCCESS = 'updatepassword/GET_DATA_REQUEST_SUCCESS'
const GET_DATA_REQUEST_FAILED = 'updatepassword/GET_DATA_REQUEST_FAILED'

export {
    GET_DATA_REQUEST,
    GET_DATA_REQUEST_SUCCESS,
    GET_DATA_REQUEST_FAILED
}