import { call, put, select } from 'redux-saga/effects'
import { URL } from '@utils/constants'

function* queryApi({ endpoint, method, body=null}) {
    const state = yield select()
    const res = yield call(makeRequest, {
        endpoint,
        method,
        headers: {
            // Authorization: state.user.accessToken ? `Bearer ${state.user.accessToken}` : null,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ ...body })
    })

    if (res.status === 401) {
        console.log('[queryApi] status 401');
    }

    const parsedResponse = yield call(parseResponse, res)

    if (!res.ok) {
        console.log('[queryApi] !res ok');
    }

    return parsedResponse
}

const makeRequest = ({ endpoint, method, headers, body=null}) => {
    console.log('ñeñeñeñe: ', URL + endpoint);
    return fetch(URL + endpoint, {
        method,
        headers,
        body: body === '{}' ? undefined : body
    })
}

const parseResponse = async response => {
    let parsedResponse;
    try {
        parsedResponse = await response.clone().json()
    } catch (error) {
        console.log(`[parseResponse] error: ${error}`);
        parsedResponse = await response.text()
    }

    return parsedResponse
}

export { queryApi }