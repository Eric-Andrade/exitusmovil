import { combineReducers } from 'redux'
import { reducer as banksReducer } from './auth/banks/reducer'

const reducer = combineReducers({
    banks: banksReducer
})

export { reducer }