import { takeEvery, put } from 'redux-saga/effects'
import { GET_DATA_REQUEST, GET_DATA_REQUEST_SUCCESS } from './actions'

function* handler() {
  yield takeEvery(GET_DATA_REQUEST, getData)
}

function* getData(action) {
  try {
    yield put({
      type: GET_DATA_REQUEST_SUCCESS,
      payload: {
        data: [],
      },
    })
  } catch (error) {
    console.log('Credits error: ', error)
  }
}

export { handler }
